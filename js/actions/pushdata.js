import { ToastAndroid } from 'react-native';

const urlPushNotification = '';

class PushData {

  constructor() {
      this.uri = global.serverUri+'/api/index.php/';
      // this.uri = 'http://192.168.1.99/api/index.php/';
      this.head = {
          credentials: 'include',
          headers: {
              'Content-Type': 'application/json',
              'User-Agent': 'mobile crosswalk',
            },
        };
    }

  data(data) {
      this.head.body = JSON.stringify(data);
      return this;
    }

  async exec() {
      try {
          const response = await fetch(this.uri, this.head);
          const responseJson = await response.json();
          return responseJson;
        } catch (error) {
          console.log('>>>', this.head.body);
            // alert('error on sync');
        }
    }

  get() {
      this.head.method = 'GET';
      return this.exec();
    }

  post() {
      this.head.method = 'POST';
      return this.exec();
    }

  endpoint(uri) {
      this.uri += uri;
      return this;
    }

}

module.exports = PushData;
