import React, { Component } from 'react';
import { View, Alert, Dimensions } from 'react-native';
import { Container, Thumbnail, Header, Title, Content, Text, Tabs, Image, Tab, Footer, Button, Icon, Left, Body, Right, Form, Label, Item, Input, Card, CardItem } from 'native-base';
import { connect } from 'react-redux';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { actions } from 'react-native-navigation-redux-helpers';
import TextField from 'react-native-md-textinput';
import DatePicker from 'react-native-datepicker';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import theme from '../../themes/base-theme';
import styles from './styles';
// import PushData from '../../actions/pushdata';
import DBconnect from '../../database/db';
import ConnectionInfo from '../../helpers/connection';

import Format from '../../helpers/Format';

import ModalBarang from './modalBarang';
import Translate from '../../helpers/translation';

const $t = new Translate().t;

const db = new DBconnect();
const sync = new ConnectionInfo();
const {
    pushRoute,
    popRoute,
} = actions;

const { DismissKeyboard } = KeyboardSpacer;
const { width } = Dimensions.get('window');


class TransactionCustomer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: global.TransactionCustomerId,
            FarmerID: '',
            StatusRevision: '',
            OrgType: '',
            OrgID: global.orgid,
            JournalID: '',
            Number: '',
            CustomerID: '',
            Date: new Date().toJSON().slice(0,10).replace(/-/g,'-'),
            Pajak: '',
            Diskon: '',
            Total: '',
            Pembayaran: '',
            SisaBayar: '',
            DateCreated: new Date().toJSON().slice(0,10).replace(/-/g,'-'),
            CreatedBy: '',
            DateUpdated: '',
            LastModifiedBy: '',
            CoopID: '',
            barang: '',
            SaleSyncID: undefined,

            modalBarang: false,
            DateStart: '',
            DateEnd: '',
            Qty: '',
            Price: '',
            indexKey: '',
            initialItems: [{ InventoryID: '', barang: '', jumlahBarang: '', price: '' }],
        };
    }

    routeNavigate(route) {
      if(!route) {
        this.props.popRoute(this.props.navigation.key);
      } else {
        this.props.pushRoute({ key: route, index: 1 }, this.props.navigation.key);
      }
    }

    async componentWillMount() {
        if(global.FarmerID){
            let q = await db.table('cd_customer').findOne({ FarmerID: global.FarmerID });
            let { Name, FarmerID } = q.item(0);
            let nameAndId = {
                id: global.TransactionCustomerId,
                FarmerID: String(FarmerID),
                CustomerID: String(FarmerID),
                FarmerName: Name,
            };
            if (global.TransactionCustomerId !== '') {
              let transaction = await db.table('cd_transaction_customer').findOne({ id: global.TransactionCustomerId });
                let transactionDetail = await db.table('cd_transaction_customer_detail').findOne({ transactionCustomerId: transaction.item(0).SaleSyncID });

                let data = transaction.item(0);
                let inventory = await db.table('cd_inventory').findOne({ id: data.InventoryID });

                let transData = {
                    id: data.id,
                    SaleSyncID: data.SaleSyncID,
                    OrgType: data.OrgType,
                    OrgID: String(data.OrgID),
                    JournalID: String(data.JournalID),
                    Number: data.Number,
                    CustomerID: String(data.CustomerID),
                    Date: data.Date,
                    Pajak: data.Pajak,
                    Diskon: data.Diskon,
                    InventoryID: data.InventoryID,
                    Pembayaran: Format.formatNumber(parseInt(data.Pembayaran)),
                    SisaBayar: Format.formatNumber(parseInt(data.SisaBayar)),
                    Total: Format.formatNumber(parseInt(data.Total)),
                    DateCreated: data.DateCreated,
                    CreatedBy: data.CreatedBy,
                    DateUpdated: data.DateUpdated,
                    LastModifiedBy: data.LastModifiedBy,
                    CoopID: String(data.CoopID),
                    Synced: data.Synced,
                }

              if (transactionDetail.length > 0) {
                  Object.assign(nameAndId, { initialItems: JSON.parse(transactionDetail.item(0).details) });
                }
                Object.assign(nameAndId, transData);
            }
            this.setState(nameAndId);
        }
    }

    get deleteButton() {
        if(global.TransactionCustomerId !== '') {
        return(
            <Col>
                <Button block danger style={{margin: 15}} onPress={() =>  this.delete()}>
                <Icon active name="md-trash" style={{ fontSize: 30, lineHeight: 32 }} />
                <Text>{$t('Delete')}</Text>
                </Button>
            </Col>
        )
        } else {
            return(<Text/>);
        }
    }

    calculatePayment() {
        setTimeout(() => {
            this.setState({
                Pembayaran: Format.formatNumber(this.state.Pembayaran),
                SisaBayar: Format.formatNumber(parseInt(this.state.Pembayaran.replace(/\./g,'')) - parseInt(this.state.Total.replace(/\./g,'')))
            })
        },10)
    }

    calculateBarang() {
        let harga;
        let fixHarga = 0;
        let stuffItems = this.state.initialItems;
        setTimeout(() => {
            stuffItems.map((prop,key) => {
                let perkalian = prop.price * prop.jumlahBarang;
                let totalHarga = { totalHarga :  perkalian };
                fixHarga += perkalian;
                Object.assign(this.state.initialItems[key],totalHarga);
            });

            let pajak = (this.state.Pajak === '') ? 0 : parseInt(this.state.Pajak);
            let diskon = (this.state.Diskon === '') ? 0 : parseInt(this.state.Diskon);
            let calculateDisc = pajak - diskon; 

            // alert(fixHarga);
            let hargaTotalQty = fixHarga;
            if(calculateDisc !== 0){
                harga = hargaTotalQty * (calculateDisc/100);
            }
            let total = hargaTotalQty+harga
            // alert(JSON.stringify());
            this.setState({
              Total: String(isNaN(total) ? Format.formatNumber(hargaTotalQty) : Format.formatNumber(total)),
            });
            this.calculatePayment();
        },30)
    }

    async save() {
        let lastNumberIDtrans = await Format.getLastID({ table: 'cd_transaction_customer' , column: 'SaleSyncID' }) 
        let GSaleId = `${global.SceID}-${lastNumberIDtrans}`;
        let data = {
            id: this.state.id,
            SaleSyncID: this.state.SaleSyncID || GSaleId,
            // OrgType: this.state.OrgType,
            OrgID: global.orgid,
            CustomerID: this.state.CustomerID,
            Date: this.state.Date,
            Pajak: this.state.Pajak,
            Diskon: this.state.Diskon,
            Total: this.state.Total.replace(/\./g, ''),
            Pembayaran: this.state.Pembayaran.replace(/\./g, ''),
            SisaBayar: this.state.SisaBayar.replace(/\./g, ''),
            DateCreated: this.state.DateCreated,
            CreatedBy: this.state.CreatedBy,
            DateUpdated: this.state.DateUpdated,
            LastModifiedBy: this.state.LastModifiedBy,
            CoopID: this.state.CoopID,
            Synced: 0,
            StatusRevision: '',
        };

        await db.table('cd_transaction_customer').save({ id: data.id }, data);

        let dataDetail = { 
          transactionCustomerId: data.SaleSyncID,
          details: JSON.stringify(this.state.initialItems),
        };

        // //add
        // let where;
        let where = (data.id == '') ? { id: '' } : where = { transactionCustomerId: data.SaleSyncID };
        await db.table('cd_transaction_customer_detail').save(where, dataDetail);
        sync.syncronize();
        this.routeNavigate();
    }

    setStateFrag(args) {
        this.setStateRecursive({ index: args.indexKey, InventoryID: args.InventoryID, barang: args.barang, price: args.Price })
        this.setState(args);
        if(args.routeTo !== undefined) {
          this.routeNavigate('inventory');
        }
    }

    delete() {
        Alert.alert(
        `Menghapus  #${this.state.id}`,
        'Apakah anda yakin ?',
        [
            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'OK', onPress: () => {
              db.table('cd_transaction_customer').save({ id: this.state.id }, { StatusRevision: 'Deleted', synced: 0 })
              this.routeNavigate();
              // db.query(`DELETE FROM cd_transaction_customer_detail WHERE transactionCustomerId = ${this.state.id};`)
                // db.query(`DELETE FROM cd_transaction_customer WHERE id = ${this.state.id};`).then(() => {
                //     // this.messageBox(`Customer id ${data.id} has been deleted`)
                // });
            }},
        ],
        { cancelable: false }
        )
    }

    cloneItem() {
        this.state.initialItems.push({ barang: '' , jumlahBarang: '' });
        this.setState({
            initialItems: this.state.initialItems,
        })
    }

    deleteRow(rowId) {
         Alert.alert(
        `Hapus Barang  #${rowId}`,
        'Apakah anda yakin ?',
        [
            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'OK', onPress: () => {
                this.state.initialItems.splice(rowId,1);
                this.setState({
                    initialItems: this.state.initialItems,
                })
                this.calculateBarang();
            }},
        ],
        { cancelable: false }
        )
    }

    setStateRecursive({ index, barang, qty, InventoryID, price }) {
        if(typeof barang !== 'undefined'){
            this.state.initialItems[index].barang = barang;
        }

        if(typeof qty !== 'undefined'){
            this.state.initialItems[index].jumlahBarang = qty;
        }

        if(typeof InventoryID !== 'undefined'){
            this.state.initialItems[index].InventoryID = InventoryID;
        }

        if(typeof price !== 'undefined'){
            this.state.initialItems[index].price = price;
        }

        this.setState({ 
            initialItems: this.state.initialItems
        });
    }

    render() {
        return(
            <Container theme={theme} style={{ backgroundColor: '#fff' }}  >
                <Header hasTabs style={{ elevation: 0, backgroundColor: '#8a6d4f' }}>
                    <Left>
                        <Button transparent onPress={() => this.routeNavigate()}>
                            <Icon active name="ios-arrow-back" />
                        </Button>
                    </Left>
                    <Body style={{ flex: 2 }}>
                        <Title><Icon name="md-create" /> {$t('Transaction')}</Title>
                    </Body>
                </Header>
                <Content padder keyboardShouldPersistTaps='always'>
                <View>
                    <ModalBarang showModal={this.state.modalBarang} setParentState={this.setStateFrag.bind(this)} indexing={this.state.indexKey} />
            <Card>
              <CardItem header>
                <Icon name="md-person" style={{color:'#4a4949'}}/>
                <Text style={{color:'#4a4949'}}>Form {$t('Transaction')}</Text>
              </CardItem>
                <CardItem>
                    <Content keyboardShouldPersistTaps='always'>
                        <Grid>
                            <Row>
                                <Col>
                                    <Row>
                                        <Col style={{ flex: 0.1, justifyContent:'center' }} >
                                            <Icon name="md-person" style={styles.iconForm} />
                                        </Col>
                                        <Col>
                                            <TextField
                                                label={$t('Farmer Name')}
                                                highlightColor={'#45be5c'}
                                                dense={true}
                                                style={{paddingBottom: 4}}
                                                value={this.state.FarmerName}
                                                onChangeText={(text) => this.state.FarmerName}
                                            />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>

                                <Col>
                                    <Row>
                                        <Col style={{ flex: 0.1, justifyContent:'center' }} >
                                            <Icon name="md-person" style={styles.iconForm} />
                                        </Col>
                                        <Col>
                                            <TextField
                                                label={$t('Farmer ID')}
                                                highlightColor={'#45be5c'}
                                                dense={true}
                                                value={this.state.FarmerID}
                                                onChangeText={(text) => this.state.FarmerID}
                                            />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col style={{flex: 0.5, justifyContent: 'center'}}>
                                    <Row>
                                        <Col style={{ flex: 0.3, justifyContent:'center' }} >
                                            <Icon name="md-calendar" style={styles.iconForm} />
                                        </Col>
                                        <Col style={{ justifyContent: 'center'}}>
                                            <Text style={{ color: '#7d7d7d', fontSize: 14, marginTop:10, marginLeft:10 }}>
                                            { $t('Date') }
                                            </Text>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col>
                                    <DatePicker
                                        style={{width: 200,top:10, marginTop:10, paddingBottom:10}}
                                        date={this.state.Date}
                                        showIcon={false}
                                        mode="date"
                                        placeholder="select date"
                                        format="YYYY-MM-DD"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                            position: 'absolute',
                                            left: 0,
                                            top: 4,
                                            marginLeft: 0
                                            },
                                            dateInput: {
                                            marginLeft: 36
                                            }
                                        }}
                                        onDateChange={(date) => {this.setState({Date: date})}}
                                        />
                                </Col>
                            </Row>
                            </Grid>
                            </Content>
                            </CardItem>

                            <CardItem header>
                                <Icon name="md-cart" style={{color:'#4a4949'}}/>
                                <Text style={{color:'#4a4949'}}>{$t('Transaction Detail')}</Text>
                            </CardItem>
                            <CardItem>
                            <Content keyboardShouldPersistTaps='always'>
                                <Grid>

                            {this.state.initialItems.map((prop,key) => {
                                return (
                                    <Row key={key}>
                                        <Col style={{flex:0.25,justifyContent:'center'}}>
                                            <Icon name="ios-basket-outline" style={styles.iconForm}/>
                                        </Col>
                                        <Col>
                                            <TextField
                                                label={$t(`Products`)}
                                                highlightColor={'#45be5c'}
                                                dense={true}
                                                value={this.state.initialItems[key].barang}
                                                onFocus={() => { this.setState({ indexKey: key, modalBarang: true })}}
                                                onChangeText={(text) => {
                                                    this.setStateRecursive({index:key , barang: text});
                                                }}
                                            />
                                        </Col>
                                        <Col style={{flex:0.24,justifyContent:'center'}}>
                                            <Icon name="md-pricetag" style={styles.iconForm}/>
                                        </Col>
                                        <Col>
                                            <TextField
                                                label={$t('Qty')}
                                                keyboardType="numeric"
                                                highlightColor={'#45be5c'}
                                                dense={true}
                                                value={this.state.initialItems[key].jumlahBarang}
                                                onChangeText={(text) => {
                                                    this.setStateRecursive({ index: key, qty: text });
                                                    this.calculateBarang()
                                                    }
                                                }
                                            />
                                        </Col>
                                        { key !== 0  && 
                                        <Col style={{ flex: 0.2 }}>
                                            <Icon name='md-close-circle' style={{color:  '#ccc' }} onPress={() => { this.deleteRow(key) }}/>
                                        </Col>
                                        }
                                    </Row>
                                    );
                                })}
                            <Row>
                                <Col>
                                <Button danger block onPress={() => this.cloneItem() }>
                                        <Text> + {$t('Add')} </Text>
                                </Button >
                                </Col>
                            </Row>
                            
                        </Grid>
                    </Content>
                </CardItem>
                <CardItem header>
                    <Icon name="md-cash" style={{color:'#4a4949'}}/>
                    <Text style={{color:'#4a4949'}}>{$t('Payment Detail')}</Text>
                </CardItem>
                <CardItem>
                    <Content keyboardShouldPersistTaps='always'>
                        <Grid>
                            {/*<Row>
                                <Col>
                                    <TextField
                                        label={$t('Journal ID')}
                                        highlightColor={'#45be5c'}
                                        dense={true}
                                        value={this.state.JournalID}
                                        onChangeText={(text) => this.setState({JournalID:text})}
                                    />
                                </Col>
                            </Row>*/}
                            {/*<Row>
                                <Col>
                                    <TextField
                                        label={$t('Number')}
                                        highlightColor={'#45be5c'}
                                        dense={true}
                                        value={this.state.Number}
                                        onChangeText={(text) => this.setState({Number:text})}
                                    />
                                </Col>
                            </Row>*/}

                            
                            <Row>
                                <Col style={{flex:0.12,justifyContent:'center'}}>
                                    <Icon name="md-pricetag" style={styles.iconForm}/>
                                </Col>
                                <Col>
                                    <TextField
                                        label={$t('Discount') + ' %'}
                                        highlightColor={'#45be5c'}
                                        keyboardType="numeric"
                                        dense={true}
                                        value={this.state.Diskon}
                                        onChangeText={(text) =>{
                                            this.setState({Diskon:text})
                                            this.calculateBarang({ Diskon: text })
                                            }
                                        } 
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col style={{flex:0.12,justifyContent:'center'}}>
                                    <Icon name="md-cash" style={styles.iconForm}/>
                                </Col>
                                <Col>
                                    <TextField
                                        label={$t('Total')}
                                        keyboardType="numeric"
                                        highlightColor={'#45be5c'}
                                        dense={true}
                                        value={this.state.Total}
                                        onChangeText={(text) => this.setState({Total:text})}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col style={{flex:0.12,justifyContent:'center'}}>
                                    <Icon name="md-cash" style={styles.iconForm}/>
                                </Col>
                                
                                <Col>
                                    <TextField
                                        label={$t('Payment')}
                                        highlightColor={'#45be5c'}
                                        keyboardType="numeric"
                                        dense={true}
                                        value={this.state.Pembayaran}
                                        onChangeText={(text) => {
                                            this.calculatePayment()
                                            this.setState({Pembayaran: Format.formatNumber(text)})
                                            }
                                        }
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col style={{flex:0.12,justifyContent:'center'}}>
                                    <Icon name="md-cash" style={styles.iconForm}/>
                                </Col>
                                <Col>
                                    <TextField
                                        label={$t('Cashback')}
                                        keyboardType="numeric"
                                        highlightColor={'#45be5c'}
                                        dense={true}
                                        value={this.state.SisaBayar}
                                        onChangeText={(text) => this.setState({SisaBayar:text})}
                                    />
                                </Col>
                            </Row>
                        </Grid>
                    </Content>
                </CardItem>
                
            </Card>
            <KeyboardSpacer/>
                </View>
                </Content>
                <Footer style={{ height: 70, backgroundColor: '#8c8c85' }} keyboardShouldPersistTaps='always' >
                <Grid>
                    <Col>
                        <Button block success style={{margin: 15}} onPress={() =>  this.save()}>
                        <Icon active name="md-create" style={{ fontSize: 30, lineHeight: 32 }} />
                        <Text>{$t('Save')}</Text>
                        </Button>
                    </Col>
                    { this.deleteButton }
                </Grid>
                </Footer>

          
            </Container>
        )
    }
}


function bindAction(dispatch) {
  return {
    popRoute: key => dispatch(popRoute(key)),
    pushRoute: (route, key) => dispatch(pushRoute(route, key)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(TransactionCustomer);
