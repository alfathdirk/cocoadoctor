import React, { Component } from 'react';
import { Image, View, Switch, Alert, Picker } from 'react-native';
import { connect } from 'react-redux';
import { Container, Header, Title, Content, Text, Button, Icon, Card, CardItem, Left, Right, Body, Footer } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import TextField from 'react-native-md-textinput';
import { actions } from 'react-native-navigation-redux-helpers';
import { openDrawer } from '../../actions/drawer';

import ModalSave from './modalPassword';

import DBconnect from '../../database/db';

import theme from './form-theme';
import styles from './styles';
import Translate from '../../helpers/translation';

const $t = new Translate().t;

const db = new DBconnect('batch');

const {
  replaceAt,
  popRoute,
} = actions;


class Profile extends Component {

  static propTypes = {
    popRoute: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }


    constructor(props) {
        super(props);
        this.state = {
          id: '',
          nama: global.realname, 			
          iddoctor: '', 			
          telepon: '', 			
          umur: '', 	
          jeniskelamin: '', 
          alamat: '',
          luaskebun: '',
          jumlahpohon: '', 
        }
    }

    
    async componentWillMount() {
      await db.query(`CREATE TABLE IF NOT EXISTS "cd_profile" ( 
        "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        "nama" TEXT,
        "iddoctor" TEXT,
        "telepon" INTEGER,
        "umur" INTEGER,
        "jeniskelamin" INTEGER,
        "alamat" TEXT,
        "luaskebun" TEXT,
        "jumlahpohon" TEXT
      )`);

        const data = await db.table('cd_profile').findAll();
        if(data.length > 0){
          let dataItem = data.item(0);
          this.setState({
            id: dataItem.id,
            nama: dataItem.nama,
            iddoctor: dataItem.iddoctor,
            telepon: String(dataItem.telepon),
            umur: String(dataItem.umur),
            jeniskelamin: dataItem.jeniskelamin,
            alamat: dataItem.alamat,
            luaskebun: dataItem.luaskebun,
            jumlahpohon: dataItem.jumlahpohon,
          });
        }
    }
    
    logout() {
      Alert.alert(
        'Logout',
        'Apakah yakin anda ingin keluar ?',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'OK', onPress: async () => {
            await db.create()
            await db.query('DROP TABLE "ktv_mobile_profile"');
            await db.query('CREATE TABLE IF NOT EXISTS "ktv_mobile_profile" ( "ProfileMAC" TEXT NOT NULL, "ProfileName" TEXT NOT NULL, "ProfileValue" TEXT )');
            global.userSession = '';
            await this.props.replaceAt('ct-account', { key: 'login' }, this.props.navigation.key);
          }},
        ],
        { cancelable: false }
      )
    }

    setStateFrag(args) {
      this.setState(args)

    }

    save() {
      const data = this.state;
      db.table('cd_profile').save({ id: this.state.id }, data);
    }

    render() {
        return (
            <Container style={{backgroundColor: '#e4f1e4'}} >
                <Image source={require('../../../images/glow2.png')} style={styles.container} >
                    <Header>
                      <Left>
                        <Button transparent onPress={this.props.openDrawer}>
                          <Icon active name="menu" />
                        </Button>
                      </Left>
                      <Body>
                        <Title>Profile</Title>
                      </Body>
                      <Right/>
                    </Header>
                    <Content padder style={{backgroundColor: 'transparent'}} >
                        <Card>
                          <CardItem>
                            <Content>

                              <TextField
                                label={$t('Name')}
                                highlightColor={'#45be5c'}
                                dense={true}
                                value={this.state.nama}
                                onChangeText={(text) => {
                                  this.setState({nama:text});
                                }}
                              />
                              <TextField
                                label={$t('CocoaDoctor ID')}
                                highlightColor={'#45be5c'}
                                dense={true}
                                value={this.state.iddoctor}
                                onChangeText={(text) => {
                                  this.setState({iddoctor:text});
                                }}
                              />

                              <TextField
                                label={$t('Phone')}
                                keyboardType="numeric"
                                highlightColor={'#45be5c'}
                                dense={true}
                                value={this.state.telepon}
                                onChangeText={(text) => {
                                  this.setState({telepon:text});
                                }}
                              />
                              <TextField
                                label={$t('Age')}
                                highlightColor={'#45be5c'}
                                keyboardType="numeric"
                                dense={true}
                                value={this.state.umur}
                                onChangeText={(text) => {
                                  this.setState({umur:text});
                                }}
                              />
                              
                              <Grid>
                                <Col style={{ justifyContent: 'center'}}>
                                  <Text style={{ color: 'black', fontSize: 13 }}>
                                    {$t('Gender')}
                                  </Text>
                                </Col>
                                <Col>
                                <Picker
                                  selectedValue={this.state.jeniskelamin}
                                  onValueChange={(itemValue, itemIndex) => this.setState({jeniskelamin: itemValue})}>
                                  <Picker.Item label={$t('Male')} value="1" />
                                  <Picker.Item label={$t('Female')} value="2" />
                                </Picker>
                                </Col>
                              </Grid>

                              <TextField
                                label={$t('Address')}
                                highlightColor={'#45be5c'}
                                dense={true}
                                value={this.state.alamat}
                                onChangeText={(text) => {
                                  this.setState({alamat:text});
                                }}
                              />
                              <TextField
                                label={$t('Garden Land Size') +  ' (Ha)'}
                                highlightColor={'#45be5c'}
                                keyboardType="numeric"
                                dense={true}
                                value={this.state.luaskebun}
                                onChangeText={(text) => {
                                  this.setState({luaskebun:text});
                                }}
                              />
                              <TextField
                                label={$t('Number of Cacao tres')}
                                highlightColor={'#45be5c'}
                                keyboardType="numeric"
                                dense={true}
                                value={this.state.jumlahpohon}
                                onChangeText={(text) => {
                                  this.setState({jumlahpohon:text});
                                }}
                              />
                            </Content>
                          </CardItem>
                        </Card>
                    </Content>
                </Image>

            <Footer style={{ height: 70, backgroundColor: '#8c8c85' }} keyboardShouldPersistTaps='always' >
                <Grid>
                    <Col>
                        <Button block success style={{margin: 15}} onPress={() =>  this.save()}>
                        <Icon active name="md-create" style={{ fontSize: 30, lineHeight: 32 }} />
                        <Text>{$t('Save')}</Text>
                        </Button>
                    </Col>
                </Grid>
                </Footer>
            </Container>
        )
    }
}

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    replaceAt: (routeKey, route, key) => dispatch(replaceAt(routeKey, route, key)),
    popRoute: key => dispatch(popRoute(key)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(Profile);
