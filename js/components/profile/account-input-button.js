import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Col, Row, Grid } from 'react-native-easy-grid';

import { closeDrawer } from '../../actions/drawer';

import {Container, Header, Title, Content, Text, Button, Icon,  Card, CardItem, InputGroup, Input, View } from 'native-base';

import theme from '../../themes/base-theme';
import styles from './styles';

class CTAccountInputButton extends Component {

    constructor(props) {
        super(props);
    }

    saveAccount() {
        this.props.popRoute();
        console.log('saveAccount');
    }

    logOut(route){
        console.log('log out');
        this.props.replaceRoute(route)
    }

    cancel(){
        this.props.popRoute();
        console.log('cancel');
    }

    render() {
        return (
            <Grid>
                <Row>
                    <Col>
                        <Button bordered  block success style={{margin: 20}} onPress={() =>this.saveAccount()} >
                            Simpan
                        </Button>
                    </Col>
                    <Col>
                        <Button bordered  block success style={{margin: 20}} onPress={() =>this.cancel()} >
                            Cancel
                        </Button>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Button bordered  block danger style={{margin: 20}} onPress={() =>this.logOut('login')} >
                            Log Out
                        </Button>
                    </Col>
                </Row>
            </Grid>
        )

    }
}

function bindAction(dispatch) {
    return {
        popRoute:()=>dispatch(popRoute()),
        replaceRoute:(route)=>dispatch(replaceRoute(route))
    }
}

export default connect(null, bindAction)(CTAccountInputButton);
