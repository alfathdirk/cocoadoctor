import React, { Component } from 'react';
import { View, ScrollView, StatusBar, ListView, TouchableHighlight } from 'react-native';

import { connect } from 'react-redux';


import { Col, Row, Grid } from 'react-native-easy-grid';
import ActionButton from 'react-native-action-button';
import { SwipeListView } from 'react-native-swipe-list-view';

import FadeInView from '../../animation/fadein';

import { actions } from 'react-native-navigation-redux-helpers';
import { Container, Thumbnail, Header, Title, Content, Text, Tabs, Image, Tab, Footer, Button, Icon, Left, Body, Right, Form, Label, Item, Input, Card, CardItem } from 'native-base';
import iconTransaction from '../../../images/icon/synced.png';

import { openDrawer } from '../../actions/drawer';
import theme from '../../themes/base-theme';
import styles from './styles';

// import PushData from '../../actions/pushdata';
import DBconnect from '../../database/db';
import iconKarung from '../../../images/icon/karung.png';
import ConnectionInfo from '../../helpers/connection';
import Translate from '../../helpers/translation';

const $t = new Translate().t;

const db = new DBconnect();
const sync = new ConnectionInfo();
const {
    pushRoute,
    popRoute,
} = actions;


class Customer extends Component {
  constructor(props) {
      super(props);
      this.state = {
          searchBar: false,
          farmers: [],
        };
      this.ds = new ListView.DataSource({
          rowHasChanged: (row1, row2) => row1 !== row2,
        });
    }

  async componentDidMount() {
    
      this.searchCustomer();
    }

  async searchCustomer(text) {
      const farmers = [];
      const query = (text) ? { Name: text, StatusRevision: '', order: { key: 'id' , sort: 'desc' } } : { StatusRevision: '', order: { key: 'id' , sort: 'desc' } };
      let dataFarmer = await db.table('cd_customer').findAll(query, text ? 1 : undefined);
      for (let i = 0; i < dataFarmer.length; i++) {
          farmers.push(dataFarmer.item(i));
        }
      this.setState({ farmers });
    }

  componentWillReceiveProps() {
      this.componentDidMount();
    }

    setStateFrag(args) {
      this.setState(args);
    }

  listFarmer() {
      this.routeNavigate('farmer');
    }

  routeNavigate(route) {
      if (!route) {
          this.props.popRoute(this.props.navigation.key);
        } else {
          this.props.pushRoute({ key: route, index: 1 }, this.props.navigation.key);
        }
    }

  messageBox() {

    }

  deleteCustomer(data) {
      const obj = {
        StatusRevision: 'Deleted',
        synced: 0,
      };
      db.table('cd_customer').save({ id: data.id }, obj);
      this.messageBox(`Customer id ${data.id} has been deleted`);
      sync.dbFarmer();
      this.componentDidMount();
    }

  farmer(item) {
      if (typeof item !== 'undefined') {
          global.FarmerID = item.FarmerID;
          global.CustomerSyncID = item.CustomerSyncID;
        } else {
          global.FarmerID = '';
          global.CustomerSyncID = '';
        }
      this.routeNavigate('farmer');
    }

  get listFarmer() {
      return (
          <View>
              <SwipeListView
              dataSource={this.ds.cloneWithRows(this.state.farmers)}
              enableEmptySections
              disableRightSwipe
              renderRow={item => (
                <TouchableHighlight onPress={() => { this.farmer(item); }}>
                  <View style={styles.rowFront}>
                    <Grid>
                      <Col>
                      {item.synced === 1 ?
                          <Icon name="md-cloud-done" style={{ color: '#3e9ab5', fontSize: 40 }} />
                        :
                        <Icon name="ios-cloud-outline" style={{ color: '#3e9ab5', fontSize: 40 }} />
                        }
                    </Col>
                      <Col size={5}>
                      <Text style={{ color: '#4d5259' }}>{ item.Name }</Text>
                      <Text style={{ color: '#4d5259', fontSize: 12 }}>{$t('Farmer ID')}: {item.FarmerID} | Group: {item.Address}</Text>
                    </Col>
                      <Col />
                    </Grid>
                  </View>
                </TouchableHighlight>
              )}
              renderHiddenRow={(data, secId, rowId, rowMap) => (
                <View style={styles.rowBack}>
                  <Thumbnail
square style={{ width: 40, height: 40 }}
                    source={iconKarung}
                  />
                  <Button danger style={{ height: 60 }} onPress={() => { this.deleteCustomer(data); }}>
                  <Icon name="md-trash" />
                </Button>
                </View>
              )}
              leftOpenValue={75}
              rightOpenValue={-57}
            />
            </View>
        );
    }
  render() {
      return (
          <Container theme={theme} style={{ backgroundColor: '#fff' }}>

          { (this.state.searchBar === false) ?
              <Header hasTabs style={{ elevation: 0, backgroundColor: '#8a6d4f' }}>
              <Left>
              <Button transparent onPress={this.props.openDrawer}>
                  <Icon active name="md-menu" />
                </Button>
            </Left>
              <Body>
              <Title><Icon name="md-list-box" /> Customer</Title>
            </Body>
              <Right>
              <Button
transparent onPress={() => {
                  this.setState({
                      searchBar: true,
                    });
                }}
                >
                  <Icon active name="search" style={{ fontSize: 30, lineHeight: 32 }} />
                </Button>
            </Right>
            </Header>
            :
            <FadeInView>
              <Header searchBar rounded>
                  <Item>
                  <Input placeholder={$t('Search by Name / ID')}  onChangeText={text => this.searchCustomer(text)} />
                  <Icon name="close" onPress={() => this.setState({ searchBar: false })} />
                </Item>
                  <Button transparent>
                  <Text>Search</Text>
                </Button>
                </Header>
            </FadeInView>
            }

          { this.listFarmer }
          <ActionButton buttonColor="#8a6d4f" style={styles.actionButton} onPress={this.farmer.bind(this)} />

        </Container>
        );
    }
}


function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    popRoute: key => dispatch(popRoute(key)),
    pushRoute: (route, key) => dispatch(pushRoute(route, key)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(Customer);
