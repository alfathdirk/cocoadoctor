import React, { Component } from 'react';
import { Picker, ToastAndroid, Dimensions, Alert, View, ListView } from 'react-native';
import { connect } from 'react-redux';
import DatePicker from 'react-native-datepicker';

import { Col, Row, Grid } from 'react-native-easy-grid';
import TextField from 'react-native-md-textinput';
import ActionButton from 'react-native-action-button';

import { actions } from 'react-native-navigation-redux-helpers';
import { Container, Badge, Header, Title, Content, Text, Tabs, Image, Tab, Footer, Button, Icon, Left, Body, Right, Form, Label, Item, Input, Card, CardItem } from 'native-base';

import { openDrawer } from '../../actions/drawer';
import theme from '../../themes/base-theme';

import DBconnect from '../../database/db';

import ConnectionInfo from '../../helpers/connection';
import Format from '../../helpers/Format';

import ModalPetani from '../search-farmer/ModalPetani';

import { Coaching, TransactionCustomer } from './activity';

import styles from './styles';
import Translate from '../../helpers/translation';

const $t = new Translate().t;

const db = new DBconnect('batch');
const sync = new ConnectionInfo();

const {
  pushRoute,
  popRoute,
} = actions;

const { width } = Dimensions.get('window');

class ListFarmer extends Component {

    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            id: '',
            farmerid: '',
            namapetani: '',
            alamat: '',
            dataSource: this.ds,
            email: '',
            phone: '',
            note: '',
            modalPetani: false,
            CustomerSyncID: undefined,
        };
    }

    async componentDidMount() {

        if(global.FarmerID !== '') {
            let query = await db.table('cd_customer').findOne({ FarmerID: global.FarmerID });
            let { VillageID, Note, Address, Phone, Email, Name, FarmerID, id, CustomerSyncID } = query.item(0);
            this.setState({
                id: id,
                farmerid: String(FarmerID),
                namapetani: Name,
                alamat: Address,
                email: Email,
                phone: Phone,
                note: Note,
                CustomerSyncID: CustomerSyncID,
            })
        }
    }

    async save() {

        let LastCustomerSyncID = await Format.getLastID({
            table: 'cd_customer',
            column: 'CustomerSyncID',
        });

        let data = {
            FarmerID: this.state.farmerid,
            CustomerSyncID: this.state.CustomerSyncID || `${global.SceID}-${LastCustomerSyncID}`,
            Name: this.state.namapetani,
            Email: this.state.email,
            Phone: this.state.phone,
            Address: this.state.alamat,
            Note: this.state.note,
            synced: 0,
            StatusRevision: '',
        }
        await db.table('cd_customer').save({ id: this.state.id }, data);
        sync.syncronize();
        this.routeNavigate();
    }

    setStateFrag(args) {
        this.setState(args);
    }

    routeNavigate(route) {
        if(!route){
            this.props.popRoute(this.props.navigation.key);
        } else {
            this.props.pushRoute({ key: route, index: 1 }, this.props.navigation.key);
        }
    }


    addCoaching() {
        global.coachingID = '';
        this.routeNavigate('coaching');
    }

    addTransactionCustomer() {
        global.TransactionCustomerId = '';
        this.routeNavigate('transactionCustomer');
    }
    
    render() {
        return(
            <Container theme={theme} style={{ backgroundColor: '#fff' }}>
                <ModalPetani showModal={this.state.modalPetani} setParentState={this.setStateFrag.bind(this)}/>
                <Header hasTabs style={{ elevation: 0, backgroundColor: '#8a6d4f' }}>
                    <Left>
                        <Button transparent onPress={() => this.routeNavigate()}>
                            <Icon active name="ios-arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title><Icon name="md-create" /> Customer</Title>
                    </Body>
                    <Right/>
                </Header>
                { (global.FarmerID !== '') ?
                <Tabs>
                    <Tab heading={$t('Data Info')} >
                    <Content padder>
                        <Card>
                            <CardItem header>
                                <Icon name="md-man" style={{ color: 'black'}}/ >
                                <Text style={{color:'black'}} >{$t('Data Information')} </Text>
                            </CardItem>
                            <CardItem>
                                <Content>
                                <Row>
                                    <Col style={{ flex: 0.12, justifyContent: 'center'}}>
                                        <Icon name="md-person" style={{color:'#ccc'}}/>
                                    </Col>
                                    <Col>
                                        <TextField
                                            label={$t('Farmer ID')}
                                            highlightColor={'#45be5c'}
                                            dense={true}
                                            value={this.state.farmerid}
                                            onFocus={() => this.setState({modalPetani: true})}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col style={{ flex: 0.12, justifyContent: 'center'}}>
                                        <Icon name="md-person" style={{color:'#ccc'}}/>
                                    </Col>
                                    <Col>
                                    <TextField
                                        label={$t('Farmer Name')}
                                        highlightColor={'#45be5c'}
                                        dense={true}
                                        value={this.state.namapetani}
                                        onChangeText={(text) => this.setState({namapetani:text})}
                                    />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col style={{ flex: 0.12, justifyContent: 'center'}}>
                                        <Icon name="md-mail" style={{color:'#ccc'}}/>
                                    </Col>
                                    <Col>
                                    <TextField
                                        label={$t('Email')}
                                        highlightColor={'#45be5c'}
                                        dense={true}
                                        value={this.state.email}
                                        onChangeText={(text) => this.setState({email:text})}
                                    />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col style={{ flex: 0.12, justifyContent: 'center'}}>
                                        <Icon name="md-call" style={{color:'#ccc'}}/>
                                    </Col>
                                    <Col>
                                        <TextField
                                            label={$t('Phone')}
                                            highlightColor={'#45be5c'}
                                            dense={true}
                                            value={this.state.phone}
                                            onChangeText={(text) => this.setState({phone:text})}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col style={{ flex: 0.12, justifyContent: 'center'}}>
                                        <Icon name="md-globe" style={{color:'#ccc'}}/>
                                    </Col>
                                    <Col>
                                        <TextField
                                            label={$t('Address')}
                                            highlightColor={'#45be5c'}
                                            dense={true}
                                            value={this.state.alamat}
                                            onChangeText={(text) => this.setState({alamat:text})}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col style={{ flex: 0.12, justifyContent: 'center'}}>
                                        <Icon name="md-book" style={{color:'#ccc'}}/>
                                    </Col>
                                    <Col>
                                        <TextField
                                            label={$t('Note')}
                                            highlightColor={'#45be5c'}
                                            dense={true}
                                            value={this.state.note}
                                            onChangeText={(text) => this.setState({note:text})}
                                        />
                                    </Col>
                                </Row>
                                </Content>
                            </CardItem>
                        </Card>
                        <Button danger block onPress={() => this.save()}>
                            <Icon active name="md-create" style={{ fontSize: 30, lineHeight: 32 }} />
                            <Text>{$t('Save')}</Text>
                        </Button>
                    </Content>
                    </Tab>
                    <Tab heading={$t('Coaching')}>
                        <Coaching routeTo={this.routeNavigate.bind(this)} />
                        <ActionButton buttonColor="#8a6d4f" style={styles.actionButton} onPress={() => { this.addCoaching() }}></ActionButton>
                    </Tab>
                    <Tab heading={$t('Transaction')}>
                        <TransactionCustomer routeTo={this.routeNavigate.bind(this)} />
                        <ActionButton buttonColor="#8a6d4f" style={styles.actionButton} onPress={() => { this.addTransactionCustomer() }}></ActionButton>
                    </Tab>
                </Tabs>
                :
                <Content padder keyboardShouldPersistTaps='always'>
                        <Card>
                            <CardItem header>
                                <Icon name="md-man" style={{ color: 'black'}}/ >
                                <Text style={{color:'black'}} >{$t('Data Information')} </Text>
                            </CardItem>
                            <CardItem>
                                <Content>
                                <Row>
                                    <Col style={{ flex: 0.12, justifyContent: 'center'}}>
                                        <Icon name="md-person" style={{color:'#ccc'}}/>
                                    </Col>
                                    <Col>
                                        <TextField
                                            label={$t('Farmer ID')}
                                            highlightColor={'#45be5c'}
                                            dense={true}
                                            value={this.state.farmerid}
                                            onFocus={() => this.setState({modalPetani: true})}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col style={{ flex: 0.12, justifyContent: 'center'}}>
                                        <Icon name="md-person" style={{color:'#ccc'}}/>
                                    </Col>
                                    <Col>
                                    <TextField
                                        label={$t('Nama Petani')}
                                        highlightColor={'#45be5c'}
                                        dense={true}
                                        value={this.state.namapetani}
                                        onChangeText={(text) => this.setState({namapetani:text})}
                                    />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col style={{ flex: 0.12, justifyContent: 'center'}}>
                                        <Icon name="md-mail" style={{color:'#ccc'}}/>
                                    </Col>
                                    <Col>
                                    <TextField
                                        label={$t('Email')}
                                        highlightColor={'#45be5c'}
                                        dense={true}
                                        value={this.state.email}
                                        onChangeText={(text) => this.setState({email:text})}
                                    />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col style={{ flex: 0.12, justifyContent: 'center'}}>
                                        <Icon name="md-call" style={{color:'#ccc'}}/>
                                    </Col>
                                    <Col>
                                        <TextField
                                            label={$t('Phone')}
                                            highlightColor={'#45be5c'}
                                            dense={true}
                                            value={this.state.phone}
                                            onChangeText={(text) => this.setState({phone:text})}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col style={{ flex: 0.12, justifyContent: 'center'}}>
                                        <Icon name="md-globe" style={{color:'#ccc'}}/>
                                    </Col>
                                    <Col>
                                        <TextField
                                            label={$t('Alamat')}
                                            highlightColor={'#45be5c'}
                                            dense={true}
                                            value={this.state.alamat}
                                            onChangeText={(text) => this.setState({alamat:text})}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col style={{ flex: 0.12, justifyContent: 'center'}}>
                                        <Icon name="md-book" style={{color:'#ccc'}}/>
                                    </Col>
                                    <Col>
                                        <TextField
                                            label={$t('Note')}
                                            highlightColor={'#45be5c'}
                                            dense={true}
                                            value={this.state.note}
                                            onChangeText={(text) => this.setState({note:text})}
                                        />
                                    </Col>
                                </Row>
                                </Content>
                            </CardItem>
                        </Card>
                        <Button danger block onPress={() => this.save()} keyboardShouldPersistTaps='always'>
                            <Icon active name="md-create" style={{ fontSize: 30, lineHeight: 32 }} />
                            <Text>{$t('Save')}</Text>
                        </Button>
                    </Content>
                }
            </Container>
        )
    }
}
function bindAction(dispatch) {
  return {
    popRoute: key => dispatch(popRoute(key)),
    pushRoute: (route, key) => dispatch(pushRoute(route, key)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(ListFarmer);
