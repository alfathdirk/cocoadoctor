const React = require('react-native');
const { StyleSheet, Platform, Dimensions } = React;
const width = Dimensions.get('window').width;

export default {
  container: {
    flex: 1,
    width: null,
    height: null,
  },
  cards: {
    alignItems: 'flex-end',
    height: 200,
  },
  card1: {
    alignItems: 'flex-end',
    height: 200,
    backgroundColor: '#f0f0f0',
  },
  card2: {
    alignItems: 'flex-end',
    height: 200,
    backgroundColor: '#dcceb8',
  },
  roundedButton: {
    alignSelf: 'center',
    marginTop: 40,
    backgroundColor: '#8a6d4f',
    borderRadius: 90,
    width: 65,
    height: 65,
    alignItems: 'center',
    justifyContent: 'center'
  },
  name: {
    color: 'red',
  },
  text: {
    marginBottom: 10,
    fontSize: 18,
  },
  closeIcon: {
    marginTop: (Platform.OS === 'ios') ? 2 : 2,
    color:'#ffffff',
    fontSize:(Platform.OS === 'ios') ? 35 : 25,
  },
  listitem:{
    borderBottomWidth:0.5,
    borderBottomColor:'#ddd',
  },
  footer: {
    width,
    // flexDirection: 'row',
    height: 155,
    // borderWidth: 0,
    // alignSelf: 'stretch',
    // alignItems: 'center',
    // justifyContent: 'space-around',
    backgroundColor: '#616D74',
  },
}
