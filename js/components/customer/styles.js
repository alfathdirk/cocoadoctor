import { Dimensions } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default {
  container: {
    flex: 1,
    width: null,
    height: null,
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: '#ffff',
  },
  footer: {
  	width: width,
  	height: 500,
  	backgroundColor: 'rgba(255,255,255,0.2)'
  },
  actionButton:{
    marginTop: 10,
  },
  roundedButton: {
    alignSelf: 'center',
    marginTop: 40,
    backgroundColor: '#8a6d4fc',
    borderRadius: 90,
    width: 65,
    height: 65,
    alignItems: 'center',
    justifyContent: 'center'
  },
  closeIcon: {
    marginTop: 2,
    color:'#ffff',
    fontSize: 25,
  },

	standalone: {
		marginTop: 30,
		marginBottom: 30,
	},
	standaloneRowFront: {
		// alignItems: 'center',
		// backgroundColor: '#CCC',
		// justifyContent: 'center',
		height: 60,
	},
	standaloneRowBack: {
		alignItems: 'center',
		backgroundColor: '#8BC645',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 15
	},
	backTextWhite: {
		color: '#FFF'
	},
	rowFront: {
    padding: 10,
		backgroundColor: '#fafafa',
		height: 60,
    borderBottomColor: '#ccd4c9',
		borderBottomWidth: 0.3,
	},
	rowBack: {
		alignItems: 'center',
		backgroundColor: '#c5ebc4',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 15,
    height:60,
	},
	backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 75
	},
	backRightBtnLeft: {
		backgroundColor: 'blue',
		right: 75
	},
	backRightBtnRight: {
		backgroundColor: 'red',
		right: 0
	},
	controls: {
		alignItems: 'center',
		marginBottom: 30
	},
	switchContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginBottom: 5
	},
	switch: {
		alignItems: 'center',
		borderWidth: 1,
		borderColor: 'black',
		paddingVertical: 10,
		width: 100,
	}
};
