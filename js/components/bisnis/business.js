import React, { Component } from 'react';
import { Alert, TextInput, Dimensions, ListView } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';

import { Container, Thumbnail, Header, Title, Content, Text, List, ListItem, Tab, Footer, Button, Icon, Left, Body, Right, Form, Label, Item, Input, Card, CardItem } from 'native-base';
import DBconnect from '../../database/db';
import Format from '../../helpers/Format';


const income = require('../../../images/income.png');
const outcome = require('../../../images/outcome.png');

const db = new DBconnect();

export default class ListBusiness extends Component {
    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            title: 'Bisnis',
            dataIncomeCVC: [],
            route: '',
        }
    }

    componentWillReceiveProps() {
        this.componentDidMount();
    }

    async componentDidMount() {
        let data = await db.table('cvc_inoutcome').findAll({ StatusRevision: '' });
        let dataIncome = []
        for (var i = 0; i < data.length; i++) {
            dataIncome.push(data.item(i));
        }
        // alert(JSON.stringify(dataIncome));
        this.setState({
            dataIncomeCVC: dataIncome,
        });
    }

    setParentState() {
        this.props.route('form-bisnis-cvc-income');
    }

    render() {
            
        return(
            <ListView
                    enableEmptySections={true}
                    dataSource={this.ds.cloneWithRows(this.state.dataIncomeCVC)}
                    renderRow={(rowData) => 
                        <List style={{ backgroundColor: '#fff' }} >
                            <ListItem style={{ height: 70}}  onPress={() => {
                                global.bisniscvcID = rowData.id;
                                global.cvcOutcome = parseInt(rowData.Kind);
                                this.setParentState();
                            }}>
                                <Grid>
                                    <Row>
                                        <Col style={{ flex: 0.5 }}>
                                        {rowData.synced == 1 ?  
                                          <Icon name="md-cloud-done" style={{ color: '#3e9ab5',fontSize: 40}}/>
                                          :
                                          <Icon name="ios-cloud-outline" style={{ color: '#3e9ab5', fontSize: 40}}/>
                                          }
                                        </Col>
                                        
                                        <Col style={{ flex: 1.3, justifyContent: 'center' }}>
                                          <Text style={{color: 'black' }}>{rowData.Tanggal}</Text>
                                        </Col>
                                        <Col style={{flexDirection: "row", flex: 1.5, justifyContent: 'center' }}>
                                          <Thumbnail
                                            style={{ height: 30, width: 30 }}
                                            source={(rowData.Kind == '1' ) ? income : outcome}
                                          />
                                            <Text style={{ textAlign: 'right', backgroundColor: '#80b3a5', padding: 7, borderRadius: 10, bottom: 6 }}>{Format.formatNumber(rowData.Total)}</Text>
                                        </Col>
                                    </Row>
                                </Grid>
                            </ListItem>
                        </List>
                    }
                />

        )
    }
}
