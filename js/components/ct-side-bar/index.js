
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, Icon, List, ListItem, Content, Thumbnail, View, Left,Body, Right, Badge } from 'native-base';
import SplashScreen from 'react-native-splash-screen';

import navigateTo from '../../actions/sideBarNav';
import styles from './style';
import imgIcon from '../../../images/icon/logo.png';

import iconTransaction from '../../../images/icon/certificate.png';
import iconHome from '../../../images/icon/home.png';
import iconProfile from '../../../images/icon/photo.png';
import iconImport from '../../../images/icon/transaksi.png';
import iconUser from '../../../images/icon/petani.png';


// const logo = require('../../../images/icon2.png');

class CTSideBar extends Component {

  navigateTo(route) {
    this.props.navigateTo(route, 'home');
  }

  translate(words) {
    return this.props.lang(words);
  }

  componentDidMount() {
    SplashScreen.hide();
  }

  homeClick() {
    if (this.props.navigation.routes[0].index !== undefined) {
      return this.navigateTo('xxx');
    }
    return this.navigateTo('home');
  }

  render() {
    return (
      <Content style={{ backgroundColor: '#e2d8c9' }} >
        <Thumbnail
          style={{ alignSelf: 'center', marginTop: 20, marginBottom: 15, resizeMode: 'contain', height: 100, width: 100 }}
          circular
          source={imgIcon}
        />

      <ListItem button onPress={this.homeClick.bind(this)} icon style={styles.links} >
            <Left>
              <Icon style={{ color: '#386346', fontSize: 30, width: 25}} name="home" />
            </Left>
            <Body>
              <Text style={{ color: '#6f6351'}} >{this.translate('Dashboard')}</Text>
            </Body>
            <Right />
          </ListItem>
          <ListItem button onPress={() => this.navigateTo('customer-menu')} icon style={styles.links} >
            <Left>
              <Icon style={{ color: '#386346', fontSize: 30, width: 25}} active name="md-people" />
            </Left>
            <Body>
               <Text style={{ color: '#6f6351'}}>{this.translate('Customer')}</Text>
            </Body>
          </ListItem>
          <ListItem button onPress={() => this.navigateTo('business')} icon style={styles.links} >
            <Left>
              <Icon style={{ color: '#386346', fontSize: 30, width: 25}} active name="paper" />
            </Left>
            <Body>
               <Text style={{ color: '#6f6351'}}>{this.translate('Business')}</Text>
            </Body>
          </ListItem>
          <ListItem button onPress={() => this.navigateTo('inventory')} icon style={styles.links} >
            <Left>
              <Icon style={{ color: '#386346', fontSize: 30, width: 25}} active name="md-albums" />
            </Left>
            <Body>
              <Text style={{ color: '#6f6351'}}>{this.translate('Products')}</Text>
            </Body>
            <Right/>
          </ListItem>
          <ListItem button onPress={() => this.navigateTo('report')} icon style={styles.links} >
            <Left>
              <Icon style={{ color: '#386346', fontSize: 30, width: 25}} active name="md-book" />
            </Left>
            <Body>
              <Text style={{ color: '#6f6351'}}>{this.translate('Report')}</Text>
            </Body>
            <Right/>
          </ListItem>
          <ListItem button onPress={() => this.navigateTo('ct-synchronization')} icon style={styles.links} >
            <Left>
              <Icon style={{ color: '#386346', fontSize: 30, width: 25}} active name="md-sync" />
            </Left>
            <Body>
              <Text style={{ color: '#6f6351'}}>{this.translate('Sync')}</Text>
            </Body>
            <Right/>
          </ListItem>
          <ListItem button onPress={() => this.navigateTo('profile')} icon style={styles.links} >
            <Left>
              <Icon style={{ color: '#386346', fontSize: 30, width: 25}} name="md-person" />
            </Left>
            <Body>
              <Text style={{ color: '#6f6351'}}>{this.translate('Profile')}</Text>
            </Body>
            <Right />
          </ListItem>
          <ListItem button onPress={() => this.navigateTo('ct-account')} icon style={styles.links} >
            <Left>
              <Icon style={{ color: '#386346', fontSize: 30, width: 25}} name="md-options" />
            </Left>
            <Body>
              <Text style={{ color: '#6f6351'}}>{this.translate('Setting')}</Text>
            </Body>
            <Right />
          </ListItem>
       </Content>
    );
  }
}


function bindAction(dispatch) {
  return {
    navigateTo: (route, homeRoute) => dispatch(navigateTo(route, homeRoute)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});


export default connect(mapStateToProps, bindAction)(CTSideBar);
