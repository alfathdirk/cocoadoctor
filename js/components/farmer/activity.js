import React, { Component } from 'react';
import { TouchableHighlight, View, ListView } from 'react-native';
import { SwipeListView } from 'react-native-swipe-list-view';
import { Col, Grid } from 'react-native-easy-grid';
import { Badge, Thumbnail, Text, Button, Icon } from 'native-base';

import styles from '../customer/styles';
import iconKarung from '../../../images/icon/karung.png';
import Format from '../../helpers/Format';

import DBconnect from '../../database/db';

const db = new DBconnect();
class TotalItem extends Component {

  constructor(props) {
    super(props);
    this.context = props;
    this.id = this.props.id;
    this.state = {
      total: '',
    };
  }

  componentWillReceiveProps() {
    this.componentDidMount();
  }

  async componentDidMount() {
    let result = await db.table('cd_transaction_customer_detail').findOne({ transactionCustomerId: this.id }); 
    let total = [];
    if(result.length > 0) {
      total = JSON.parse(result.item(0).details);
    }
    this.setState({
      total: String(total.length),
    });
  }

  render() {
    return(
        <Grid>
          <Col>
              <Text style={{ color: '#4d5259' }}>Item: {this.state.total}</Text>
          </Col>
        </Grid>
          );
  }
}

export class Coaching extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: '',
            coachingList: [],
        };
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    }

    async componentDidMount() {
        let data = await db.table('cd_coaching').findAll({ FarmerID: global.FarmerID, StatusRevision: '' });
        let coachings = [];
        for (let i = 0; i < data.length; i++) {
            coachings.push(data.item(i));
        }
        this.setState({
            coachingList: coachings,
        });
    }


    componentWillReceiveProps(props) {
        this.componentDidMount();
    }

    goToCoaching(item) {
        global.coachingID = item.id;
        this.props.routeTo('coaching');
    }
    
    get listCoaching() {
        return(
            <View>
            <SwipeListView
              dataSource={this.ds.cloneWithRows(this.state.coachingList)}
              enableEmptySections={true}
              disableRightSwipe={true}
              disableLeftSwipe={true}
              renderRow={ item => (
                <TouchableHighlight onPress={ () => {  this.goToCoaching(item) }}>
                  <View style={styles.rowFront}>
                    <Grid>
                      <Col>
                        {item.synced == 1 ?  
                        <Icon name="md-cloud-done" style={{ color: '#3e9ab5',fontSize: 40}}/>
                        :
                        <Icon name="ios-cloud-outline" style={{ color: '#3e9ab5', fontSize: 40}}/>
                        }
                      </Col>
                    <Col size={5}>
                      <Text style={{ color: '#4d5259' }}>[ { item.tanggalCoaching } ] - {item.issue}</Text>
                    </Col>
                    </Grid>
                  </View>
                  </TouchableHighlight>
              )}
              renderHiddenRow={ (data, secId, rowId, rowMap) => (
                <View style={styles.rowBack}>
                  <Thumbnail square style={{width: 40, height: 40}}
                    source={iconKarung}
                  />
                <Button danger style={{ height:60 }} onPress={() => { this.deleteCustomer(data) }}>
                    <Icon name="md-trash"/>
                  </Button>
                </View>
              )}
              leftOpenValue={75}
              rightOpenValue={-57}
            />
            </View>
        );
    }

    render() {
        return this.listCoaching;
    }
}

export class TransactionCustomer extends Component {

    constructor(props) {
        super(props)
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        this.state = {
          transactionList: [],
        }

    }

    async componentDidMount() {
      let transactions = [];
      let data = await db.table('cd_transaction_customer').findAll({ CustomerID: global.FarmerID, StatusRevision: '' });
      for (var i = 0; i < data.length; i++) {
        transactions.push(data.item(i));
      }
      this.setState({
        transactionList: transactions,
      });
    }

    componentWillReceiveProps(props) {
        this.componentDidMount();
    }

    goToTransaction(item) {
      global.TransactionCustomerId = item.id;
      this.props.routeTo('transactionCustomer');
    }

    render() {
      return(
        <View>
          <SwipeListView
            dataSource={this.ds.cloneWithRows(this.state.transactionList)}
            enableEmptySections={true}
            disableRightSwipe={true}
            disableLeftSwipe={true}
            renderRow={ item => (
              <TouchableHighlight onPress={ () => {  this.goToTransaction(item) }}>
                <View style={styles.rowFront}>
                  <Grid>
                  <Col style={{flex: 0.3}}>
                      {item.synced == 1 ?  
                      <Icon name="md-cloud-done" style={{ color: '#3e9ab5',fontSize: 40}}/>
                      :
                      <Icon name="ios-cloud-outline" style={{ color: '#3e9ab5', fontSize: 40}}/>
                      }
                      
                    </Col>
                  <Col>
                    <Text style={{ color: '#4d5259' }}>[ {item.Date} ] </Text>
                    <TotalItem id={item.SaleSyncID} />
                  </Col>
                  <Col style={{ justifyContent: 'center' }}>
                    <Text style={{ textAlign: 'right', color: '#fff', backgroundColor: '#3e9ab5', padding: 7, borderRadius:10 }}>Rp {Format.formatNumber(item.Total)} </Text>
                  </Col>
                  </Grid>
                </View>
                </TouchableHighlight>
            )}
            renderHiddenRow={ (data, secId, rowId, rowMap) => (
              <View style={styles.rowBack}>
                <Thumbnail square style={{width: 40, height: 40}}
                  source={iconKarung}
                />
              <Button danger style={{ height:60 }} onPress={() => { this.deleteCustomer(data) }}>
                  <Icon name="md-trash"/>
                </Button>
              </View>
            )}
            leftOpenValue={75}
            rightOpenValue={-57}
          />
        </View> 
      );
    }
}
