import React, { Component } from 'react';
import { Image, View, ToastAndroid, ListView, Alert, TouchableHighlight } from 'react-native';
import { Container, Content, Text, Right, Card, CardItem, Icon, Header, Left, Title, Body, Button, Spinner } from 'native-base';
import { connect } from 'react-redux';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { actions } from 'react-native-navigation-redux-helpers';
import { Buffer } from 'buffer';

import { openDrawer } from '../../actions/drawer';

import DBconnect from '../../database/db';
import styles from './styles';
import theme from '../../themes/base-theme';
import iconDownload from '../../../images/icon/download.png';
import iconUpload from '../../../images/icon/upload.png';
import PushData from '../../actions/pushdata';
import Translate from '../../helpers/translation';

const $t = new Translate().t;
const db = new DBconnect('batch');

const {
  popRoute,
  pushRoute,
} = actions;

class Loading extends Component {

  constructor(props) {
    super(props);
    this.state = {
      downloading: '0',
      totalLength: this.props.totalLength,
    };
  }

  render() {
    if(this.props.onsync !== false){
      return(
        <Grid>
          <Col>
            <Card>
              <CardItem>
                <Spinner color='green' />
                <Text style={{color:'#969696'}}> Downloading farmer {this.props.download}/{this.props.totalLength}</Text>
              </CardItem>
            </Card>
          </Col>
        </Grid>
      );
    } else {
      return(false);
    }
  }
}

class CTSync extends Component {

  static propTypes = {
    popRoute: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }

  constructor(props) {
    super(props);
    this.state = {
      title : 'Sync',
      sync: false,
      download: '0',
      totalLength: '0',
    }
  }

  insertToDB(data) {
    setTimeout(function() {
      // body...
      this.insertToDB()
    }, 200);
  }

  async syncDownload() {
    let that = this;
    // let body = { sid: 74 };
    // db.query(`DELETE FROM "ktv_cocoa_farmer"`);

    let body = { sid: global.supplychainid, pid: global.pid , uid: global.userid };
    const head = {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Authorization': global.tokenLogin,
        // 'Authorization': 'Basic '+new Buffer('jebekoko:jebekoko').toString('base64'),
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    };
    // uri = 'http://192.168.56.1/api/index.php/sync/traceabilitydownloadfarmer';
    uri = global.serverUri+'/api/index.php/sync/scedownloadfarmer';
    try {
      await db.create();
      let response = await fetch(uri,head);
      let responseJson = await response.json();
      that.setState({
        totalLength: responseJson.data.length,
      });

      let i = 0;
      let si = setInterval(() => {
        if(i >= responseJson.data.length - 1){
          clearInterval(si);
        }
          let dataFarmer = {
            FarmerID: responseJson.data[i].FarmerID,
            FarmerName: responseJson.data[i].FarmerName.replace(/\'/g," "),
            CPGid: responseJson.data[i].CPGid,
            GroupName: responseJson.data[i].CPGid,
            VillageID: responseJson.data[i].VillageID,
            VillageName: (responseJson.data[i].Village != null) ? responseJson.data[i].Village.replace(/\'/g," ") : responseJson.data[i].Village != null,
            Photo: (responseJson.data[i].Photo != null ? responseJson.data[i].Photo.replace(/\'/g," ") : responseJson.data[i].Photo ),
            Quota: responseJson.data[i].Kuota,
            DateCreated: responseJson.data[i].DateCreated,
            CreatedBy: responseJson.data[i].CreatedBy,
            DateUpdated: responseJson.data[i].DateUpdated,
            isCertified: responseJson.data[i].isCertified,
            LastModifiedBy: responseJson.data[i].LastModifiedBy,
          }
          db.table('ktv_cocoa_farmer').save({ id : '' }, dataFarmer);
          i++
      })

      let check = setInterval(function() {
        // alert(i);
        if(i >= responseJson.data.length - 1) {
          clearInterval(check)
          that.setState({
            sync: false
          });
          ToastAndroid.showWithGravity(
            `Download Farmer ${responseJson.data.length} Success`,
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
          );
        }
        that.setState({
          download: String(i + 1),
        })
      }, 3000);
      // for (var i = 0; i < responseJson.data.length; i++) {
      // }
    } catch(error) {
      console.log(error);
      ToastAndroid.showWithGravity(
        `Error on Sync ${error}`,
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
    }
  }

  async goSync(data, table) {
    for (let i = 0; i < data.length; i++) {
      delete data[i].InventoryID;
      delete data[i].CreatedTime;
      delete data[i].id;
      delete data[i].CoachingID;
      delete data[i].WowIncomeID;
      delete data[i].SaleId;
      delete data[i].details;
      delete data[i].WowOutcomeID;
      data[i].synced = 1;
      v = Object.values(data[i]);
      k = Object.keys(data[i]);
      if(v[i] == null) {
        data[i][k[i]] = '';
      }
      await db.table(table).save({ id: '' }, data[i]);
    }
    this.setState({
      sync: false,
    });
  }

  async goSyncTransactionAndDetail(data) {
    // console.log('vvv',data)
    for (let i = 0; i < data.length; i++) {
      let dataTransDetail = data[i].details.map(async (v,k) => {
        let keys = Object.keys(v);
        for (let i = 0; i < keys.length; i++) {
          switch (keys[i]) {
            case 'InventoryID':
              v.InventoryID = v[keys[i]];
              break;
            case 'Qty':
              v.jumlahBarang = v[keys[i]];
              delete v[keys[i]]
              break;
            case 'TotalPrice':
              v.totalHarga = parseInt(v[keys[i]]);
              delete v[keys[i]]
              break;
            default:
              delete v[keys[i]]
              break;
            }
            v.price = parseInt(v.totalHarga) / parseInt(v.jumlahBarang);
            let barang = await db.table('cd_inventory').findOne({ InventorySyncID: v.InventoryID });
            v.barang = barang.item(0).Name;
          }
        return v
      });

      Promise.all(dataTransDetail).then(async (details) => {
        const objDetails = {
          transactionCustomerID: data[i].SaleSyncID,
          details: JSON.stringify(details),
        };
        await db.table('cd_transaction_customer_detail').save({ id: '' }, objDetails);
      });
    }
    await this.goSync(data,'cd_transaction_customer');
  }

  async syncUpload() {
    const { data: inventory } = await new PushData().endpoint('cocoadoctor/downloadInventory/'+global.SceID).data().get();
    const { data: customer } = await new PushData().endpoint('cocoadoctor/downloadCustomer/'+global.SceID).data().get();
    const { data: businesCVC } = await new PushData().endpoint('cocoadoctor/downloadCvc/'+global.SceID).data().get();
    const { data: wowIn } = await new PushData().endpoint('cocoadoctor/downloadWowIncome/'+global.SceID).data().get();
    const { data: wowOut } = await new PushData().endpoint('cocoadoctor/downloadWowOutcome/'+global.SceID).data().get();
    const { data: coaching } = await new PushData().endpoint('cocoadoctor/downloadCoaching/'+global.SceID).data().get();
    const { data: transaction } = await new PushData().endpoint('cocoadoctor/downloadSale/'+global.SceID).data().get();

    await this.goSync(inventory,'cd_inventory');
    await this.goSync(customer,'cd_customer');
    await this.goSync(coaching,'cd_coaching');
    await this.goSync(wowIn,'wow_income');
    await this.goSync(wowOut,'wow_outcome');
    await this.goSync(businesCVC,'cvc_inoutcome');
    await this.goSyncTransactionAndDetail(transaction);

  }

  sync(type){
    this.setState({
      sync: true
    })
    if(type) {
      this.syncDownload();
    } else {
      this.syncUpload();
    }
  }

  popRoute() {
    return this.props.popRoute(this.props.navigation.key);
  }

  render(){
    return (
      <Container theme={theme} style={{ backgroundColor: '#fff' }}>
        <Header style={{ elevation: 0, backgroundColor: '#8a6d4f' }}>
          <Left>
            <Button transparent onPress={this.props.openDrawer}>
              <Icon active name="menu" />
            </Button>
          </Left>
          <Body>
            <Title><Icon name="md-sync" /> {this.state.title}</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Grid>
            <Col>
              <Card>
                <CardItem
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <View>
                    <TouchableHighlight onPress={() => this.sync()} >
                      <Image source={iconUpload} style={{ width: 100, height: 100 }} />
                    </TouchableHighlight>
                    <Text style={{ color: '#2fba7c' }}> {$t('Restore Data')}</Text>
                  </View>
                </CardItem>
              </Card>
            </Col>
            
            <Col>
              <Card>
                <CardItem
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <View>
                    <TouchableHighlight onPress={() =>  this.sync('download')} >
                      <Image source={iconDownload} style={{ width: 100, height: 100 }} />
                    </TouchableHighlight>
                    <Text style={{ color: '#2fba7c' }}> {$t('Download Farmer')}</Text>
                  </View>
                </CardItem>
              </Card>
            </Col>
          </Grid>
          <Loading onsync={this.state.sync} download={this.state.download} totalLength={this.state.totalLength}/>
        </Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    popRoute: key => dispatch(popRoute(key)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(CTSync);
