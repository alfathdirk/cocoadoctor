import DBconnect from '../database/db';

class Format {
  leftPad([...val], lengthVal, separator){
    const digit = [...separator.repeat(lengthVal)];
    digit.splice(digit.length - val.length, val.length, val.join(''));
    return digit.join('');
  }

  getLastID({ table, column }) {
      return new DBconnect().table(table).findLast(column).then((result) => {
        let results = result.item(0);
        if(typeof results !== 'undefined') {
          let SaleId = results[column].split('-')[1];
          return parseInt(SaleId)+1;
        }
        return '1';
      })
  }

  formatNumber (num) {
    if(num) {
      const number = num.toString().replace(/\./g,'');
      return number.toString().replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1" + '.');
    }
    return '';
  }

}

module.exports = new Format();
