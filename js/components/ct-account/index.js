import React, { Component } from 'react';
import { Image, View, Switch, Alert, Picker, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Container, Header, Title, Content, Text, Button, Icon, Card, CardItem, Left, Right, Body } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import TextField from 'react-native-md-textinput';
import { actions } from 'react-native-navigation-redux-helpers';
import { openDrawer } from '../../actions/drawer';
import RNRestart from 'react-native-restart'; // Import package from node modules

// Immediately reload the React Native Bundle
import ModalSave from './modalPassword';

import DBconnect from '../../database/db';
import Translate from '../../helpers/translation';


import theme from './form-theme';
import styles from './styles';

const $t = new Translate().t;
const db = new DBconnect('batch');

const {
  replaceAt,
  popRoute,
} = actions;


class CTAccount extends Component {

  static propTypes = {
    popRoute: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }


    constructor(props) {
        super(props);
        this.state = {
            server: global.serverUri ,
            email: 'info@koltiva.com',
            ffPrice: '',
            faqPrice: '',
            safeMode: false,
            moisture: '8',
            seed: '115',
            fungus: '2',
            pest: '3',
            grease: '99',
            dirt: '3',
            language: 'en',
            showDialogForm: false,
        }
    }

    logout() {
      Alert.alert(
        'Logout',
        'Apakah yakin anda ingin keluar ?',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'OK', onPress: async () => {
            await db.create()
            await db.query('DROP TABLE "ktv_mobile_profile"');
            await db.query('CREATE TABLE IF NOT EXISTS "ktv_mobile_profile" ( "ProfileMAC" TEXT NOT NULL, "ProfileName" TEXT NOT NULL, "ProfileValue" TEXT )');
            global.userSession = '';
            await this.props.replaceAt('ct-account', { key: 'login' }, this.props.navigation.key);
            RNRestart.Restart();

          }},
        ],
        { cancelable: false }
      )
    }

    setStateFrag(args) {
      this.setState(args)

    }

    async componentWillMount () {
      let lang = await AsyncStorage.getItem('language');
      if(lang === null) {
        lang = 'en';
      }
      this.setState({
        language: lang,
      });
    }

    async save() {
      if(global.serverUri !== this.state.server) {
        this.setState({
          showDialogForm: true,
        });
      }
      await AsyncStorage.setItem('language', this.state.language);
      RNRestart.Restart();
      return;
    }

    render() {
        return (
            <Container theme={theme} style={{backgroundColor: '#e4f1e4'}} >
                <Image source={require('../../../images/glow2.png')} style={styles.container} >
                    <Header>
                      <Left>
                        <Button transparent onPress={this.props.openDrawer}>
                          <Icon active name="menu" />
                        </Button>
                      </Left>
                      <Body>
                        <Title>Setting</Title>
                      </Body>
                      <Right/>
                    </Header>
                    <Content padder style={{backgroundColor: 'transparent'}} >
                        <Card transparent foregroundColor="#000">

                            <CardItem header>
                              <Text style={{ color: '#717171', fontStyle: 'italic' }}>{$t('Configuration')}</Text>
                            </CardItem>
                            <CardItem>
                              <Content>
                                <TextField
                                    label={'Server'}
                                    highlightColor={'#45be5c'}
                                    dense={true}
                                    value={this.state.server}
                                    onChangeText={(text) => {
                                      this.setState({server:text});
                                    }}
                                />
                              </Content>
                            </CardItem>

                            <CardItem>
                              <Content>
                                <TextField
                                    label={'Email'}
                                    highlightColor={'#45be5c'}
                                    dense={true}
                                    value={this.state.email}
                                    onChangeText={(text) => this.setState({email:text})}
                                />
                              </Content>
                            </CardItem>

                            <CardItem>
                              <Content>
                                <Text style={{ color: '#676767', fontSize: 13 }}>
                                  {$t('Language')}
                                </Text>
                                <Picker
                                  selectedValue={this.state.language}
                                  onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
                                  <Picker.Item label="Indonesia" value="id" />
                                  <Picker.Item label="English" value="en" />
                                </Picker>
                              </Content>
                            </CardItem>

                            <CardItem>
                              <Content>
                              <Button block success style={{margin: 15}} onPress={() => {
                                this.save();
                              }}>
                              <Icon active name="ios-add-circle" />
                                <Text>Save</Text>
                              </Button>
                              <Button block danger style={{margin: 15}} onPress={() => {
                                this.logout();
                              }}>
                              <Icon active name="md-close-circle" />
                                <Text>{$t('Logout')}</Text>
                              </Button>
                              </Content>
                            </CardItem>
                        </Card>
                    </Content>
                    <ModalSave visible={this.state.showDialogForm} setParentState={this.setStateFrag.bind(this)} server={this.state.server} />
                </Image>
            </Container>
        )
    }
}

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    replaceAt: (routeKey, route, key) => dispatch(replaceAt(routeKey, route, key)),
    popRoute: key => dispatch(popRoute(key)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(CTAccount);
