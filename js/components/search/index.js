import React, { Component } from 'react';
import SearchBar from 'react-native-material-design-searchbar';
import FadeInView from '../../animation/fadein';

export default class SearchElement extends Component {
  render() {
    if(this.props.searchBarShow === true){
      return (
        <FadeInView>
          <SearchBar
            onSearchChange={() => console.log('On Focus')}
            height={30}
            onFocus={() => console.log('On Focus')}
            onBlur={() => console.log('On Blur')}
            placeholder={'Search...'}
            autoCorrect={false}
            padding={5}
            returnKeyType={'search'}
          />
        </FadeInView>
      );
    } else {
      return(null)
    }

  }
}
