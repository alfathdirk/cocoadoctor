import React, { Component } from 'react';
import { Alert, TextInput, Dimensions, ScrollView } from 'react-native';

import { Col, Row, Grid } from 'react-native-easy-grid';
import TextField from 'react-native-md-textinput';

import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';

import { Container, Thumbnail, Header, View, Title, Content, Text, Tabs, Image, Tab, Footer, Button, Icon, Left, Body, Right, Form, Label, Item, Input, Card, CardItem } from 'native-base';
import theme from '../../themes/base-theme';
import styles from './styles';

import Format from '../../helpers/Format';
import DBconnect from '../../database/db';
import Translate from '../../helpers/translation';

const $t = new Translate().t;

const db = new DBconnect();

const { width } = Dimensions.get('window');

export default class ProfitLoss extends Component {

  constructor(props) {
    super(props);
    this.state = {
      anu: 'asdf',
      totalAmount: 0,
      dataTrans: [],
      Transaction: 0,
      IncomeWow: 0,
      OutcomeWow: 0,
      Outcome: 0,
      totalOutcome: 0,
      totalIncome: 0,
      totalProfitLoss: 0,
    };
  }

  async componentWillMount() {
    const getTrans = await this.props.dataProfitLoss();
    const totalIncome = getTrans.Transaction + getTrans.IncomeWow;
    const totalOutcome = getTrans.OutcomeWow + getTrans.Outcome;

    Object.assign(getTrans, { 
      totalIncome,
      totalOutcome,
      totalProfitLoss: totalIncome - totalOutcome,
    });

    this.setState(getTrans);
  }

  render() {
    return(
      <View>
        <View style={{ height: 90 }}>
            <Grid style={{ borderBottomWidth: 0.3, borderBottomColor: '#ccc' }}>
              <Row>
                <Col style={{ justifyContent: 'center', flex: 0.2 }}>
                  <Icon style={{ color: '#795a5a', left: 10 }} active name="md-calendar" />
                </Col>
                <Col style={{ flex: 0.4, justifyContent: 'center' }}>
                  <Text style={{ color: 'black', fontWeight: 'bold', fontStyle: 'italic' }}>{$t('Date Range')}</Text>
                </Col>
                <Col style={{justifyContent: 'center'}}>
                  <Text style={{ color: 'black', textAlign: 'right', marginRight: 20, fontWeight: 'bold', fontStyle: 'italic', fontSize: 12 }}>{this.props.transactionDate}</Text>
                </Col>
              </Row>

              <Row style={{ backgroundColor: '#a08467' }}>
                <Col style={{ justifyContent: 'center' }}>
                  <Text style={{textAlign: 'center' }}>
                    {$t('Account')}
                  </Text>
                </Col>
                <Col style={{ justifyContent: 'center' }}>
                  <Text style={{textAlign: 'center' }}>
                    {$t('Amount')}
                  </Text>
                </Col>
              </Row>
            </Grid>
        </View>
        <ScrollView>
          <Content>
            <Grid>
              <Card style={{ bottom: 5 }}>
                <CardItem header>
                  <Text style={{ color: '#5a5a5a', fontStyle: 'italic', fontWeight: 'bold' }}>{$t('Revenue')}</Text>
                </CardItem>
                <CardItem>
                  <Grid>
                    <Row>
                      <Col style={{ justifyContent: 'center' }}>
                        <Text style={{ color: 'black' }}>
                          {$t('Sales Transaction')}
                        </Text>
                      </Col>
                      <Col style={{ justifyContent: 'center' }}>
                        <Text style={{ color: 'black', textAlign: 'right' }}>
                          { Format.formatNumber(this.state.Transaction) }
                        </Text>
                      </Col>
                    </Row>
                    <Row>
                      <Col style={{ justifyContent: 'center' }}>
                        <Text style={{ color: 'black' }}>
                          {$t('Wow Farm')}
                        </Text>
                      </Col>
                      <Col style={{ justifyContent: 'center' }}>
                        <Text style={{ color: 'black', textAlign: 'right' }}>
                          { Format.formatNumber(this.state.IncomeWow) }
                        </Text>
                      </Col>
                    </Row>
                  </Grid>
                </CardItem>
                <CardItem header>
                  <Text style={{ color: '#5a5a5a', fontStyle: 'italic', fontWeight: 'bold' }}>{$t('Expense')}</Text>
                </CardItem>
                <CardItem>
                  <Grid>
                    <Row>
                      <Col style={{ justifyContent: 'center' }}>
                        <Text style={{ color: 'black' }}>
                          {$t('Sales Expense')}
                        </Text>
                      </Col>
                      <Col style={{ justifyContent: 'center' }}>
                        <Text style={{ color: 'black', textAlign: 'right' }}>
                          { Format.formatNumber(this.state.Outcome) }
                        </Text>
                      </Col>
                    </Row>
                    <Row>
                      <Col style={{ justifyContent: 'center' }}>
                        <Text style={{ color: 'black' }}>
                          {$t('Wow Farm')} 
                        </Text>
                      </Col>
                      <Col style={{ justifyContent: 'center' }}>
                        <Text style={{ color: 'black', textAlign: 'right' }}>
                          { Format.formatNumber(this.state.OutcomeWow) }
                        </Text>
                      </Col>
                    </Row>
                  </Grid>
                </CardItem>
                <CardItem footer>
                  <Text style={{ color: '#5a5a5a', fontStyle: 'italic', fontWeight: 'bold' }}>{$t('Profit & Loss')}</Text>
                  <Right>
                    <Text style={{ color: '#fff', backgroundColor: (this.state.totalProfitLoss < 0) ? '#ce6a6a' : '#6acec0' , padding: 10, borderRadius: 15 }}>
                      {Format.formatNumber(this.state.totalProfitLoss)}
                    </Text>
                  </Right>
                </CardItem>
              </Card>
            </Grid>
          </Content>
        </ScrollView>
      </View>
    );
  }
}
