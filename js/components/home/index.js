import React, { Component } from 'react';
import { Image, Dimensions, Alert, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { actions } from 'react-native-navigation-redux-helpers';
import { Container, Header, Title, Content, Button, Icon, Card, CardItem, Text, Left, Right, Body, Thumbnail, Footer, H2, Badge, H3, View } from 'native-base';
import { Col, Grid } from 'react-native-easy-grid';

import ActionButton from 'react-native-action-button';
import DatePicker from 'react-native-datepicker';
import Profile from '../../helpers/profile';
import Translate from '../../helpers/translation';

import { openDrawer } from '../../actions/drawer';

import styles from './styles';
import DBconnect from '../../database/db';

const { width } = Dimensions.get('window');

const db = new DBconnect('batch');
const {
  reset,
} = actions;

const glow2 = require('../../../images/glow2.png');
const petani = require('../../../images/icon/petani.png');
const kebun = require('../../../images/icon/kebun.png');
const pohon = require('../../../images/icon/pohon.png');
const trafic = require('../../../images/icon/chart.png');
const alfath = require('../../../images/icon/h.jpg');

const today = new Date().toJSON().slice(0, 10).replace(/-/g, '-');
const $t = new Translate().t;
// logout
// <Button
//  style={styles.roundedButton}
//  onPress={() => this.props.reset(this.props.navigation.key)}
// ></Button><Icon active name="close" style={styles.closeIcon} />

class Home extends Component {  //eslint-disable-line

  static propTypes = {
    openDrawer: React.PropTypes.func,
    reset: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }

  constructor(props) {
    super(props);
    this.state = {
      petaniCertified: '0',
      petaniNonCertified: '0',
      statusOpen: '',
      statusClose: '',
      statusSend: '',
      totalTrans: '',
      TotalAmount: '',
      totalBruto: '',
      DateStart: today,
      DateEnd: today,
    };

      // const intervalId = BackgroundTimer.setTimeout(() => {
      //   console.log('tic');
      // }, 3000);
      //
      // BackgroundTimer.clearTimeout(intervalId);
  }

  async componentWillMount() {
    // this.reloadData([today, today]);
    
    await Profile.getAllProfile().then((res) => {
      for (let i = 0; i < res.length; i++) {
        switch (res[i].ProfileMAC.toLowerCase()) {
          case 'partnerid':
            global.pid = res[i].ProfileValue;
            break;
          case 'supplychainid':
            global.supplychainid = res[i].ProfileValue;
            break;
          case 'name':
            global.name = res[i].ProfileValue;
            break;
          case 'realname':
            global.realname = res[i].ProfileValue;
            break;
          case 'orgtype':
            global.orgtype = res[i].ProfileValue;
            break;
          case 'userid':
            global.userid = res[i].ProfileValue;
            break;
          case 'orgid':
            global.orgid = res[i].ProfileValue;
            global.SceID = global.orgid;
            break;
          case 'token':
            global.tokenLogin = res[i].ProfileValue;
            break;
          default:
            break;
        }
      }
    });
  }

  componentDidMount() {
    this.getFarmerCertified();
    this.getFarmerNonCertified();
  }

  async getFarmerCertified() {
    const counter = await db.query('SELECT count(isCertified) as c FROM ktv_cocoa_farmer where isCertified = 1').then((result) => result.item(0));
    this.setState({
      petaniCertified: counter.c,
    });
  }

  async getFarmerNonCertified() {
    const counter = await db.query('SELECT count(isCertified) as c FROM ktv_cocoa_farmer where isCertified = 0').then((result) => result.item(0));
    this.setState({
      petaniNonCertified: counter.c,
    });
  }
  setDate(date) {
    this.setState(date);
    const start = date.DateStart || this.state.DateStart;
    const end = date.DateEnd || this.state.DateEnd;
    this.reloadData([start, end]);
  }

  async reloadData(date) {
    // this.getTotalTransaction(date);
    // this.getTotalAmount(date);
    // this.getTotalBruto(date);
    // this.setState({
    //   statusOpen: await this.getBatchStatus('Open'),
    //   statusClose: await this.getBatchStatus('Close'),
    //   statusSend: await this.getBatchStatus('Terkirim'),
    // });
  }

  render() {
    return (
      <Container style={{ backgroundColor: '#fff' }}>
        <Image source={glow2} style={styles.container} >

          <Header style={{ backgroundColor: '#8a6d4f' }}>
            <Left>
              <Button transparent onPress={this.props.openDrawer} >
                <Icon active name="menu" style={{ fontSize: 30, lineHeight: 32 }} />
              </Button>
            </Left>
            <Body>

              <Title><Icon name="md-home" /> Home</Title>
            </Body>
            <Right />
          </Header>

          <View style={{ height: 75 }}>
              <Card>
                <CardItem>
                  <Content>
                    <Grid>
                      <Col>
                        <DatePicker
                        style={{ width: width / 2.5 + 20 }}
                        date={this.state.DateStart}
                        mode="date"
                        placeholder="Date From"
                        format="YYYY-MM-DD"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        showIcon
                        onDateChange={
                              (date) => {
                                this.setDate({ DateStart: date });
                              }
                          }
                      />
                      </Col>
                      <Col>
                        <DatePicker
                        style={{ width: width / 2.5 + 20 }}
                        date={this.state.DateEnd}
                        mode="date"
                        placeholder="Date To"
                        format="YYYY-MM-DD"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        showIcon
                        onDateChange={
                              (date) => { this.setDate({ DateEnd: date }) ;}
                          }
                      />
                      </Col>
                    </Grid>
                  </Content>
                </CardItem>
              </Card>
            </View>
          <Content>
            <Card style={styles.cards}>
              <CardItem style={styles.card2}>
                <Body>
                  <Grid>

                    <Col style={{ width: 120 }}>
                      <Thumbnail
                        style={{ height: 100, width: 100, margin: 10 }}
                        source={petani}
                      />
                      <Text style={{ color: '#525252', alignSelf: 'center', textAlign: 'center' }}>{ $t('Number of Farmers') }</Text>
                    </Col>
                    <Col style={{ marginTop: 20 }}>
                      <Grid>
                        <Col>
                          <Text style={{ fontSize: 15, color: '#525252', alignSelf: 'center', fontStyle: 'italic' }}>
                            { $t('Certified') }
                          </Text>
                          <Badge success style={{ alignSelf: 'center', marginTop: 20 }}>
                            <Text>
                              {this.state.petaniCertified}
                            </Text>
                          </Badge>
                        </Col>
                        <Col>
                          <Text style={{ fontSize: 15, color: '#525252', alignSelf: 'center', fontStyle: 'italic' }}>
                            { $t('Not Certified') }
                          </Text>
                          <Badge danger style={{ alignSelf: 'center', marginTop: 20 }}>
                            <Text>
                              {this.state.petaniNonCertified}
                            </Text>
                          </Badge>
                        </Col>
                      </Grid>
                    </Col>
                  </Grid>
                </Body>
              </CardItem>
            </Card>
            <Card style={styles.cards}>
              <CardItem style={styles.card1} >
                <Body>
                  <Grid>
                    <Col style={{ width: 120 }}>
                      <Thumbnail
                        style={{ height: 100, width: 100, margin: 10 }}
                        source={pohon}
                      />
                      <Text style={{ color: '#525252', alignSelf: 'center', marginTop: 10 }}>{$t('Transaction')}</Text>
                    </Col>
                    <Col>
                      <Text style={{ fontSize: 12, color: '#525252', alignSelf: 'center', fontStyle: 'italic', marginTop: 10 }}>
                        {$t('Transaction')}
                      </Text>
                      <Badge info style={{ alignSelf: 'center', marginTop: 20 }}>
                        <Text>{this.state.totalTrans}</Text>
                      </Badge>
                    </Col>
                    <Col>
                      <Text style={{ fontSize: 12, color: '#525252', alignSelf: 'center', fontStyle: 'italic', marginTop: 10 }}>
                      { $t('Weight')}
                      </Text>
                      <Badge info style={{ alignSelf: 'center', marginTop: 20 }}>
                        <Text>{this.state.totalBruto} Kg</Text>
                      </Badge>
                    </Col>
                    <Col>
                      <Text style={{ fontSize: 12 , color: '#525252', alignSelf: 'center', fontStyle: 'italic', marginTop: 10 }}>
                        {$t('Amount')}
                      </Text>
                      <Badge info style={{ alignSelf: 'center', marginTop: 20 }}>
                        <Text>{this.state.TotalAmount}</Text>
                      </Badge>
                    </Col>
                  </Grid>
                </Body>
              </CardItem>
            </Card>
          </Content>
        </Image>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    reset: key => dispatch(reset([{ key: 'login' }], key, 0)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(Home);
