import React, { Component } from 'react';
import SQLite from 'react-native-sqlite-storage';

const db = SQLite.openDatabase('cocoatrace.db', '1.0', 'Cocoa Database', 200000, this.openCB, this.errorCB);
SQLite.enablePromise(true);
// SQLite.DEBUG(true);

export default class DBconnect extends Component {
  constructor(dbName) {
    super(dbName);
    this.config = {};
    this.tableName = '';
  }

  openCB() {
    console.log('db connect');
  }

  errorCB(err) {
    console.error('err on connect db', err);
  }

  query(query) {
    return new Promise((resolve, reject) => {
      db.transaction((tx) => {
        tx.executeSql((query), [], (tx, results) => {
          resolve(results.rows);
        });
      });
    });
  }

  table(tableName) {
    const that = this;
    return {
      save: (key, data) => that._save(key,tableName,data),
      findAll: (obj, like) => {
        const objId = obj || '';
        return that._findAll(objId, tableName, like);
      },
      findOne: (id) => that._findOne(id,tableName),
      findMax: (column) => new Promise((resolve,reject) => {
          db.transaction((tx) => {
            tx.executeSql((`SELECT MAX(${column}) as ${column}Max FROM ${tableName} `),[],(tx,results) => {
              resolve(results.rows);
            });
          });
        }),
      findLast: (column) => new Promise((resolve, reject) => {
          db.transaction((tx) => {
            tx.executeSql((`SELECT * FROM ${tableName} order by ${column} desc LIMIT 1`),[],(tx,results) => {
              resolve(results.rows);
            });
          });
        }),
    };
  }

  _findOne(id, tableName) {
    const field = Object.keys(id)[0];
    const value = Object.values(id)[0];
    return new Promise((resolve, reject) => {
      db.transaction((tx) => {
        tx.executeSql((`SELECT * FROM ${tableName} WHERE ${field}=? LIMIT 1;`), [value], (tx, results) => {
          resolve(results.rows);
        });
      });
    });
  }

  _findAll(objId, tableName, like) {
    let where = '';
    let limit = '';
    let order = '';

    const data = new Promise((resolve, reject) => {
      if (objId) {
        const keyObject = Object.keys(objId);
        for (let i = 0; i < keyObject.length; i++) {
          const val = objId[keyObject[i]] == null ? 'is null' : `= '${objId[keyObject[i]]}'`;
          if (keyObject[i] != 'limit' && keyObject[i] != 'order') {
            if (i > 0) {
              where += ` AND ${keyObject[i]} ${val}`;
            } else {
              where += `WHERE ${keyObject[i]} ${val}`;
            }
          }
        }

        if (typeof objId.limit !== 'undefined') {
          limit = `LIMIT ${objId.limit}`;
        }

        if (typeof objId.order !== 'undefined') {
          order = `ORDER BY ${objId.order.key} ${objId.order.sort}`;
        }

        if (keyObject.length < 1) {
          where = '';
        }
      }

      let exec = `SELECT * FROM ${tableName} ${where} ${order} ${limit}`;
      if (like) {
        const z = exec.replace(/=/g, 'LIKE');
        const x = z.replace(/(\w+')/g, '%$1%\'');
        exec = x.replace(/\'\%\'/g, '%\'');
      }
      db.transaction((tx) => {
        tx.executeSql(exec).then(([tx, results]) => {
          const data = results.rows;
          resolve(data);
        }).catch((error) => {
          console.log(error);
        });
      });
    });
    return data;
  }

  create() {
    this._populatesDB();
  }

  _save(key, tableName, data) {
    const keyField = Object.keys(key)[0];
    const valueField = Object.values(key)[0];
    const values = [];
    const structure = [];
    for (var key in data) {
      values.push(String(data[key]));
      structure.push(key);
    }
    db.transaction((tx) => {
      // add
      if (valueField === '' || valueField === undefined) {
        const indexId = structure.findIndex((el) => {
          if (el === keyField) {
            return el;
          }
        });
        if (indexId !== -1) {
          structure.splice(indexId, 1);
          values.splice(indexId, 1);
        }
        const query = `INSERT INTO ${tableName} (${structure.join()}) VALUES ('${values.join('\',\'')}');`;
        console.log(query);
        tx.executeSql(query);
      } else {
        const inlineQuery = [];
        for (let i = 0; i < structure.length; i++) {
          inlineQuery[i] = `${structure[i]} = '${values[i]}'`;
        }
        const query = `UPDATE ${tableName} SET ${inlineQuery} WHERE ${keyField} = "${valueField}";`;
        tx.executeSql(query);
      }
    });
  }

  _populatesDB() {
    const that = this;
    console.log('populated ...');
    db.transaction((tx) => {
      tx.executeSql(`DROP TABLE IF EXISTS "ktv_cocoa_farmer"`);
      tx.executeSql('CREATE TABLE IF NOT EXISTS "ktv_cocoa_farmer" ( "FarmerID" INTEGER NOT NULL, "FarmerName" TEXT(255), "CPGid" INTEGER, "GroupName" TEXT(255), "VillageID" INTEGER, "VillageName" TEXT(255), "isCertified" INTEGER, "Photo" TEXT(255), "Quota" REAL, "DateCreated" TEXT(255), "CreatedBy" INTEGER, "DateUpdated" TEXT(255), "LastModifiedBy" INTEGER, PRIMARY KEY ("FarmerID" ASC) )').catch((error) => {
        that.errorCB(error);
      });
      tx.executeSql('CREATE TABLE IF NOT EXISTS "user" ( "userID" TEXT NOT NULL, "username" TEXT )').catch((error) => {
        that.errorCB(error);
      });
      tx.executeSql('CREATE TABLE IF NOT EXISTS "ktv_setting" ( "serverUri" TEXT, "email" TEXT )').catch((error) => {
      });

      tx.executeSql('CREATE TABLE IF NOT EXISTS "ktv_mobile_profile" ( "ProfileMAC" TEXT NOT NULL, "ProfileName" TEXT NOT NULL, "ProfileValue" TEXT )').catch((error) => {
        that.errorCB(error);
      });

      tx.executeSql(`CREATE TABLE IF NOT EXISTS "cd_inventory" (
        "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        "InventoryID" INTEGER(5) DEFAULT NULL,
        "InventorySyncID" INTEGER(5) DEFAULT NULL,
        "OrgType" INTEGER(5) DEFAULT NULL,
        "Status" INTEGER(5) DEFAULT NULL,
        "OrgID" INTEGER(11) DEFAULT NULL,
        "CoopID" INTEGER(11) DEFAULT NULL,
        "JournalID" INTEGER(10) DEFAULT NULL,
        "Number" TEXT(30) DEFAULT NULL,
        "SerialNumber" TEXT(100) DEFAULT NULL,
        "Name" TEXT(30) DEFAULT NULL,
        "Description" TEXT(225) DEFAULT NULL,
        "UnitMeasurementID" INTEGER(11) DEFAULT NULL,
        "IsInventory" INTEGER(1) DEFAULT NULL,
        "IsSell" INTEGER(1) DEFAULT NULL,
        "IsBuy" INTEGER(1) DEFAULT NULL,
        "IsRemoved" INTEGER(1) DEFAULT NULL,
        "RemoveReason" TEXT(225) DEFAULT NULL,
        "Price" INTEGER(11) DEFAULT NULL,
        "DateCreated" TEXT DEFAULT NULL,
        "Stock" INTEGER(11) DEFAULT NULL,
        "synced" INTEGER(1) DEFAULT NULL,
        "StatusRevision" TEXT(255) DEFAULT NULL
        )`);

        tx.executeSql(`DROP TABLE IF EXISTS "cvc_inoutcome"`);
        tx.executeSql(`CREATE TABLE IF NOT EXISTS "cvc_inoutcome" (
          "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
          "cvcSyncID" TEXT(255) ,
          "OrgID" INTEGER(11) ,
          "CoopID" INTEGER(11) ,
          "Tanggal" TEXT(225) ,
          "Uraian" TEXT ,
          "Jumlah" TEXT ,
          "Unit" INTEGER(1) ,
          "Harga" TEXT ,
          "Total" TEXT ,
          "Kind" TEXT ,
          "synced" INTEGER(1),
          "StatusRevision" TEXT(255) DEFAULT NULL
          )`);

        tx.executeSql('drop table IF Exists wow_income');
        tx.executeSql(`CREATE TABLE IF NOT EXISTS "wow_income" (
            "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            "wowInSyncID" TEXT(11),
            "OrgID" INTEGER(11),
            "TanggalPanen" TEXT(225),
            "HariKering" TEXT(225),
            "BijiKering" TEXT(225),
            "BijiBasah" TEXT(225),
            "TanggalPenjualan" TEXT(225),
            "Harga" TEXT(225),
            "Total" TEXT,
            "Keterangan" TEXT,
            "synced" INTEGER(1),
            "StatusRevision" TEXT(255) DEFAULT NULL
            )`);  


          tx.executeSql('DROP TABLE IF EXISTS "wow_outcome"');
           tx.executeSql(`CREATE TABLE IF NOT EXISTS "wow_outcome" (
            "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            "wowOutSyncID" TEXT(11),
            "OrgID" INTEGER(11),
            "CoopID" INTEGER(11),
            "Tanggal" TEXT(225),
            "Uraian" TEXT,
            "Jumlah" TEXT,
            "Unit" INTEGER(1),
            "Harga" TEXT,
            "Total" TEXT,
            "synced" INTEGER(1),
            "StatusRevision" TEXT(255) DEFAULT NULL
            )`);

        tx.executeSql('DROP TABLE IF EXISTS "cd_coaching"');
        tx.executeSql(`CREATE TABLE IF NOT EXISTS "cd_coaching" ( 
          "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
          "CoachingSyncID" TEXT ,
          "CustomerSyncID" TEXT ,
          "FarmerID" INTEGER,
          "FarmerName" TEXT(255),
          "tanggalCoaching" TECT(255),
          "luasLahan" TEXT(255),
          "notes" TEXT(255),
          "cvcImplement" TEXT(255),
          "issue" TEXT(255),
          "recomendation" TEXT(255),
          "dateend" TEXT(255),
          "synced" INTEGER,
          "StatusRevision" TEXT(20) DEFAULT NULL
          )`);

          tx.executeSql('DROP TABLE IF EXISTS "cd_customer"');
          tx.executeSql(`CREATE TABLE IF NOT EXISTS "cd_customer" ( 
              "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
              "FarmerID" INTEGER ,
              "CustomerSyncID" TEXT(255),
              "SceID" TEXT(255),
              "Name" TEXT(255),
              "Email" TEXT(255),
              "Phone" TEXT(255),
              "Address" TEXT(255),
              "Note" TEXT(255),
              "VillageName" TEXT,
              "VillageID" INTEGER,
              "StatusRevision" TEXT(20) DEFAULT NULL,
              "synced" INTEGER 
          )`);
          
          tx.executeSql('DROP TABLE IF EXISTS "cd_transaction_customer"');
          tx.executeSql('DROP TABLE IF EXISTS "cd_transaction_customer_detail"');
          tx.executeSql(`
          CREATE TABLE IF NOT EXISTS "cd_transaction_customer_detail" (
              "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
              "transactionCustomerID" INTEGER NOT NULL,
              "details" TEXT DEFAULT NULL
            )`);
          tx.executeSql(`
          CREATE TABLE IF NOT EXISTS "cd_transaction_customer" (
              "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
              "SaleSyncID" TEXT NOT NULL,
              "OrgType" TEXT,
              "OrgID" INTEGER(11),
              "JournalID" INTEGER(11),
              "Number" INTEGER,
              "CustomerID" INTEGER(11),
              "Date" TEXT,
              "Pajak" TEXT,
              "Diskon" TEXT,
              "Total" TEXT,
              "Pembayaran" TEXT,
              "SisaBayar" TEXT,
              "DateCreated" TEXT,
              "CreatedBy" INTEGER(11),
              "DateUpdated" TEXT,
              "LastModifiedBy" INTEGER(11),
              "CoopID" INTEGER(10),
              "synced" INTEGER,
              "Problem" TEXT(250) DEFAULT NULL,
              "Solution" TEXT(250) DEFAULT NULL,
              "DateStart" TEXT DEFAULT NULL,
              "DateEnd" TEXT DEFAULT NULL,
              "Qty" TEXT DEFAULT NULL,
              "Price" TEXT DEFAULT NULL,
              "StatusRevision" TEXT(255) DEFAULT NULL
          )`);
    });
  }
}
