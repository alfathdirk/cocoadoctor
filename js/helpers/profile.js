import DBconnect from '../database/db';

const db = new DBconnect();


class Profile {
  constructor() {

  }

  async getSupplyChainID() {
    let supplychainid = await db.query(`Select ProfileValue From ktv_mobile_profile where ProfileMAC = 'supplychainid'`).then((result) => {
      return result.item(0).ProfileValue;
    });
    return supplychainid;
  }

  getToken() {

  }
  async getAllProfile(){
    let profile = await db.query(`Select * From ktv_mobile_profile`).then((result) => {
      let obj = [];
      for (var i = 0; i < result.length; i++) {
        obj.push(result.item(i));
      }
      return obj;
    });
    return profile;
  }

  getOrgID() {

  }

  getName() {

  }

}

module.exports = new Profile();
