import React, { Component } from 'react';
import { Picker, Modal, Alert } from 'react-native';
import { Container, Body, Footer, Left, Title, Right, Content, Label, Text, Button, View, Card, CardItem, Thumbnail, Header, Icon } from 'native-base';
import TextField from 'react-native-md-textinput';

import { Col, Row, Grid } from 'react-native-easy-grid';
import styles from './styles';
import DBconnect from '../../database/db';
import Format from '../../helpers/Format';

import badges from '../../../images/icon/xx.png';
import ConnectionInfo from '../../helpers/connection';
import Translate from '../../helpers/translation';

const $t = new Translate().t;

const db = new DBconnect();
const sync = new ConnectionInfo();

export default class ModalForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: '',
      InventorySyncID: '',
      OrgType: '',
      Status: '',
      OrgID: global.SceID,
      CoopID: '',
      JournalID: '',
      Number: '',
      SerialNumber: '',
      Name: '',
      Description: '',
      UnitMeasurementID: '',
      Price: '',
      DateCreated: '',
      Stock: '',
      synced: 0,
      StatusRevision: '',
    };
  }

  async getData(id) {
      let state = {} ;
      let value = '';
      if(id !== '') {
        const data = await db.table('cd_inventory').findOne({ id: id });
        data.item(0).Stock = String(data.item(0).Stock);
        data.item(0).Price = String(data.item(0).Price);
        data.item(0).Status = String(data.item(0).Status);
        data.item(0).OrgID = global.SceID;
        data.item(0).synced = 0;
        data.item(0).StatusRevision = '';
        state = data.item(0); 
      } else {
        for (let key in this.state) {
          switch (key) {
            case 'synced':
              value = 0;
              break;
            case 'OrgID':
              value = global.SceID;
              break;
            case 'DateCreated':
              value = new Date().toJSON().slice(0,10).replace(/-/g,'-');
              break;
            case 'UnitMeasurementID':
              value = '0';
              break;
            case 'Stock':
              value = '0';
              break;
            case 'Status':
              value = '0';
              break;
            case 'StatusRevision':
              value = '';
              break;
            default:
              value = '';
              break;
          }
          Object.assign(state,{[key]: value})
        }
      }
      this.setState(state)
  }

  get deleteButton() {
    return(
      <Col>
          <Button block danger style={{margin: 15}} onPress={() =>  this.delete(this.props.inventoryID)}>
            <Icon active name="md-create" style={{ fontSize: 30, lineHeight: 32 }} />
            <Text>Delete</Text>
          </Button>
      </Col>
    )
  }

  setParentState(args) {
    this.props.setParentState(args)
  }

  async save() {
    if(this.state.id == '') {
      let lastInventoryID = await Format.getLastID({ table: 'cd_inventory' , column: 'InventorySyncID' }) 
      await this.setState({
        InventorySyncID: `${global.SceID}-${lastInventoryID}`,
      });
    }
    await db.table('cd_inventory').save({ id: this.state.id }, this.state);
    this.setParentState({ showModal: false, refresh: true });
    sync.dbInventory();
  }

  onModalShow() {
    this.getData(this.props.inventoryID);
  }

  render() {
    return(
      <Modal
        onShow={this.onModalShow.bind(this)}
        animationType={"slide"}
        transparent={true}
        visible={this.props.showModal}
        onRequestClose={() => {this.setParentState({showModal: false})}}
        >
      <Container style={{ backgroundColor: '#fafafa' }}>
          <Header style={{ elevation: 0, backgroundColor: '#8a6d4f' }}>
            <Left>
                <Button transparent onPress={() => this.setParentState({showModal: false})}>
                    <Icon active name="ios-arrow-back" />
                </Button>
            </Left>
            <Body>
              <Title><Icon name="md-basket" /> {$t('Products')}</Title>
            </Body>
            <Right />
          </Header>
          <Content padder style={{ backgroundColor: 'transparent' }}>
            <Card>
              <CardItem header>
                <Icon style={{color: '#555555'}} name="md-basket" />
                <Text style={{color: '#555555'}}>{$t('Products')}</Text>
              </CardItem>
              <CardItem>
                <Content>
                  <Grid>
                    <Row>
                      <Col>
                        <TextField
                            label={$t('Name')}
                            highlightColor={'#45be5c'}
                            dense={true}
                            value={this.state.Name}
                            onChangeText={(text) => {
                              this.setState({ Name: text })
                            }}
                        />
                      </Col>
                    </Row>
                  <Row>
                  <Col>
                  <TextField
                      label={$t('Price')}
                      highlightColor={'#45be5c'}
                      keyboardType="numeric"
                      dense={true}
                      value={this.state.Price}
                      onChangeText={(text) => {
                        this.setState({ Price: text })
                      }}
                  />
                  </Col>
                  </Row>
                  <Row>
                  <Col>
                  <TextField
                      label={$t('Stock')}
                      highlightColor={'#45be5c'}
                      dense={true}
                      keyboardType="numeric"
                      value={this.state.Stock}
                      onChangeText={(text) => {
                        this.setState({ Stock: text })
                      }}
                  />
                  </Col>
                  </Row>
                  <Row>
                  <Col>
                    <Col style={{ justifyContent: 'center'}}>
                      <Text style={{ color: '#777575', fontSize:14 }}>
                        {$t('Unit Type')}
                      </Text>
                    </Col>
                  </Col>
                   <Col>
                      <Picker
                        selectedValue={this.state.UnitMeasurementID}
                        onValueChange={(itemValue, itemIndex) => this.setState({UnitMeasurementID: itemValue})}>
                        <Picker.Item label="Kilogram" value="0" />
                        <Picker.Item label="Liter" value="1" />
                        <Picker.Item label="Unit" value="2" />
                        <Picker.Item label="Packet" value="3" />
                      </Picker>
                    </Col>
                  </Row>
                  <Row>
                  <Col>
                  <TextField
                      label={$t('Serial Number')}
                      highlightColor={'#45be5c'}
                      dense={true}
                      value={this.state.SerialNumber}
                      onChangeText={(text) => {
                        this.setState({ SerialNumber: text })
                      }}
                  />
                  </Col>
                  </Row>
                  <Row>
                  <Col>
                  <TextField
                      label={$t('Description')}
                      highlightColor={'#45be5c'}
                      dense={true}
                      value={this.state.Description}
                      onChangeText={(text) => {
                        this.setState({ Description: text })
                      }}
                  />
                  </Col>
                  </Row>
                  <Row>
                    <Col style={{ justifyContent: 'center'}}>
                      <Text style={{ color: '#777575', fontSize:14 }}>
                        Status
                      </Text>
                    </Col>
                    <Col>
                      <Picker
                        selectedValue={this.state.Status}
                        onValueChange={(itemValue, itemIndex) => this.setState({Status: itemValue})}>
                        <Picker.Item label={$t('Active')} value="1" />
                        <Picker.Item label={$t('Inactive')} value="0" />
                      </Picker>
                    </Col>
                  </Row>
                  </Grid>
              </Content>
            </CardItem>
            </Card>
          </Content>
            <Footer style={{ height: 70, backgroundColor: '#8c8c85' }} keyboardShouldPersistTaps='always' >
                <Grid>
                    <Col>
                        <Button block success style={{margin: 15}} onPress={() =>  this.save()}>
                        <Icon active name="md-create" style={{ fontSize: 30, lineHeight: 32 }} />
                        <Text>{$t('Save')}</Text>
                        </Button>
                    </Col>
                    { this.props.inventoryID !== '' ? this.deleteButton : <Text/>}
                </Grid>
                </Footer>
        </Container>

      </Modal>
    );
  }
}
