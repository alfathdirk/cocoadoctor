import { Dimensions } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default {
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: '#ffff',
  },
  actionButton:{
    marginTop: 10,
  }
}