import React, { Component } from 'react';
import { Picker, Modal, Alert, ListView, ScrollView } from 'react-native';
import { Container,Footer, Body, List, ListItem, Right, Content, Input, Label, Text, Button, View, Card, Item, CardItem, Thumbnail, Header, Icon } from 'native-base';
import TextField from 'react-native-md-textinput';
import { Col, Row, Grid } from 'react-native-easy-grid';

import styles from './styles';
import DBconnect from '../../database/db';

import badges from '../../../images/icon/xx.png';

const db = new DBconnect('batch');


export default class ModalBarang extends Component {

    constructor(props) {
        super(props);
        this.state = {
            namaBarang: [],
            query: '',
        }
    }

    componentWillReceiveProps() {
      this.searchBarang(10)
    }

    async searchBarang(limit,nama) {
        let barangs = [];
        let propsLimit = { limit: limit };
        if(nama) {
            Object.assign(propsLimit,{ Name: nama });
        }
        
        let barang = await db.table('cd_inventory').findAll(propsLimit,1);
        for (let i = 0; i < barang.length; i++) {
            barangs.push(barang.item(i));
        }
        this.setState({
            namaBarang: barangs,
        });
    }
    
    setParentState(arg) {
        this.props.setParentState(arg);
    }

    setItem(item) {
        this.setParentState({
            barang: item.Name,
            indexKey: this.props.indexing,
            InventoryID: item.InventorySyncID,
            Price: item.Price,
            modalBarang: false,
        });
    }

    satuan(type) {
      let unit;
      switch (type) {
        case 0:
          unit = '/Kg';
          break;
        case 1:
          unit = '/Ltr';
          break;
        case 2:
          unit = '/Unit';
          break;
        case 2:
          unit = '/Pkg';
          break;
        default:
          break;
      }
      return unit;
    }
    render() {

        return(
            <Modal
                animationType={"slide"}
                transparent={true}
                visible={this.props.showModal}
                onRequestClose={() => {this.setParentState({ modalBarang: false})}}
                >
                <ScrollView keyboardShouldPersistTaps="always">
            <Container style={{ backgroundColor: '#fafafa' }}>
                <Header searchBar style={{ elevation: 0, backgroundColor: '#8a6d4f' }}>
                    <Item>
                        <Icon name="ios-search" />
                        <Input onChangeText={(text) => this.searchBarang(10,text) } placeholder="Search" />
                        <Icon name="ios-people" />
                    </Item>
                    <Button transparent>
                        <Text>Search</Text>
                    </Button>
                </Header>
                <Content keyboardShouldPersistTaps='always'>
                    <List keyboardShouldPersistTaps='always'
                        dataArray={this.state.namaBarang}
                        renderRow={(item) =>
                        <ListItem style={{ height: 60}} onPress={() => {
                                this.setItem(item);
                            }}>
                            <Grid>
                                <Col>
                                    <Text style={{color: 'black'}}>{item.Name}</Text>
                                </Col>
                                <Col>
                                    <Text style={{ color: 'black' }}>
                                        
                                    </Text>
                                </Col>
                                <Col>
                                    <Text style={{color: 'black'}}>{item.Price}{this.satuan(item.UnitMeasurementID)}</Text>
                                </Col>
                            </Grid>
                        </ListItem>
                        }>
                    </List>
                    </Content>
                <Footer style={{ height: 70, backgroundColor: '#8c8c85' }} keyboardShouldPersistTaps='always' >
                <Grid>
                    <Col>
                        <Button block warning style={{margin: 15}} onPress={() =>  this.setParentState({ routeTo: 'inventory', modalBarang: false })}>
                        <Icon active name="ios-add" style={{ fontSize: 30, lineHeight: 32 }} />
                        <Text>Add</Text>
                        </Button>
                    </Col>
                    <Col>
                        <Button block danger style={{margin: 15}} onPress={() =>  this.setParentState({ modalBarang: false })}>
                        <Icon active name="md-close" style={{ fontSize: 30, lineHeight: 32 }} />
                        <Text>Cancel</Text>
                        </Button>
                    </Col>
                </Grid>
                </Footer>
                </Container>
                </ScrollView>
            </Modal>
        )
    }
}
