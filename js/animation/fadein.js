import React, { Component } from 'react';
import { Animated } from 'react-native';

class FadeInView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fadeAnim: new Animated.Value(0),
    };
  }
  componentDidMount() {
    Animated.timing(
      this.state.fadeAnim,
      { toValue: 1 }
    ).start();
  }
  render() {
    return (
      <Animated.View
        style={{opacity: this.state.fadeAnim, transform: [{
         translateX: this.state.fadeAnim.interpolate({
           inputRange: [0, 1],
           outputRange: [150, 0],
         }),
       }]}}>
        {this.props.children}
      </Animated.View>
    );
  }
}

module.exports = FadeInView;
