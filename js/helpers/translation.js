import I18n from 'react-native-i18n';
import { en, id } from '../language/lang';

I18n.translations = { en, id };

export default class Translate {

  t(words) {
    Object.assign(en, {
        [words]: words,
    });
    return I18n.t(words);
  }
}