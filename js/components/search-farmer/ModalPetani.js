import React, { Component } from 'react';
import { Modal } from 'react-native';
import { Container, Body, Left, Right, Content, Text, Button, Footer, View, List, ListItem, Thumbnail, Header, Input, Icon, Item } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import styles from './styles';
import DBconnect from '../../database/db';
import BarcodeScannerApp from './Barcode';

import iconFarmer from '../../../images/icon/petani.png';
import badges from '../../../images/icon/xx.png';
import Translate from '../../helpers/translation';

const $t = new Translate().t;
const db = new DBconnect('batch');

class CertifiedOrNot extends Component {
  render() {
    if(this.props.badge === 1){
      return(
        <View>
            <Thumbnail square style={{width: 40, height: 40, justifyContent: 'center'}}
              source={badges}
            />
        </View>
      );
    } else {
      return(false)
    }
  }
}

export default class ModalPetani extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      showCamera: false,
      farmer: [],
    };
  }

  setParentState(status) {
    this.props.setParentState(status);
  }

  getDataFarmer(FarmerName) {
    let farmer, farmerbyId;
    if(FarmerName){
      let FarmerNames = (isNaN(FarmerName) === true) ? 'FarmerName' : 'FarmerID';
      farmer = db.table('ktv_cocoa_farmer').findAll({[FarmerNames]:FarmerName, limit: 20},1);
    } else {
      farmer = db.table('ktv_cocoa_farmer').findAll( {limit: 20} );
    }
    farmer.then(resultFarmer => {
      let datafarmer = [];
      for (var i = 0; i < resultFarmer.length; i++) {
        datafarmer[i] = resultFarmer.item(i);
      }
      this.setState({ farmer: datafarmer});
    })
  }

  componentWillMount() {
    this.getDataFarmer();
  }

  selectedFarmer(item) {
    this.setParentState({
      farmerid: String(item.FarmerID),
      namapetani: item.FarmerName,
      // GroupName: item.GroupName,
      alamat: item.VillageName,
      modalPetani: false,
    });
  }

  async scanBarcode(args) {
    let { data, type } = args.e;
    let result = await db.table('ktv_cocoa_farmer').findOne({ FarmerID: data }).then((result) => {
      return result.item(0);
    })
    await this.setState({showCamera: args.showCamera})
    await this.selectedFarmer(result);
  }

  render() {
    return(
      <Modal
        animationType={"slide"}
        transparent={true}
        visible={this.props.showModal}
        onRequestClose={() => {this.setState({modalPetani: false})}}
        keyboardShouldPersistTaps="always"
        >
      <Container style={{ backgroundColor: '#fafafa' }} keyboardShouldPersistTaps="always">
          <Header searchBar rounded>
            <Item>
              <Input placeholder={$t('Search by Name / ID')} onChangeText={(text) => this.getDataFarmer(text)}/>
              <Icon name="search" />
              <Icon
                name="md-qr-scanner" onPress={() => {
                  this.setState({showCamera: true});
              }}
              />
            </Item>
          </Header>
          <Content padder style={{ backgroundColor: 'transparent' }} keyboardShouldPersistTaps="always">
            <BarcodeScannerApp showCamera={this.state.showCamera} setParentState={this.scanBarcode.bind(this)}/>
            <List
              keyboardShouldPersistTaps="always"
              dataArray={this.state.farmer}
              renderRow={(item) =>
                <ListItem icon style={styles.listitem} onPress={() => this.selectedFarmer(item)} keyboardShouldPersistTaps="always">

                    <Body>
                      <Text style={{ color: '#4d5259' }}>[{item.FarmerID}] - {item.FarmerName}</Text>
                      <Text style={{ color: '#4d5259', fontSize: 11 }}>Group : {item.GroupName} - {$t('Village')} : {item.VillageName}</Text>
                    </Body>
                    <Right>
                      <CertifiedOrNot badge={item.isCertified} />
                    </Right>
                </ListItem>
              }>
            </List>
          </Content>
            <Footer style={{ height: 45 }}>
              <Grid>
                <Col>
                  <Button
                    block
                    success
                    onPress={()=>{
                      this.setParentState({modalPetani: false});
                    }} >
                    <Icon name="close" />
                    <Text>{$t('Hide')}</Text>
                  </Button>
                </Col>
              </Grid>
            </Footer>
        </Container>

      </Modal>
    );
  }
}
