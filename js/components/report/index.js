import React, { Component } from 'react';
import { Alert, TextInput, Dimensions, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { actions } from 'react-native-navigation-redux-helpers';
import TextField from 'react-native-md-textinput';
import DatePicker from 'react-native-datepicker';
import ActionButton from 'react-native-action-button';

import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';

import { Container, Thumbnail, Header, View, Title, Spinner, Content, Text, Tabs, Image, Tab, Footer, Button, Icon, Left, Body, Right, Form, Label, Item, Input, Card, CardItem } from 'native-base';
import theme from '../../themes/base-theme';
import styles from './styles';
// import PushData from '../../actions/pushdata';
import DBconnect from '../../database/db';

import Format from '../../helpers/Format';
import ConnectionInfo from '../../helpers/connection';

import { openDrawer } from '../../actions/drawer';

import ProfitLoss from './profitloss';
import TransactionIncome from './transaction';
import IncomeWow from './incomeWow';
import Expense from './expense';
import ExpenseWow from './expenseWow';
import Translate from '../../helpers/translation';

const $t = new Translate().t;

const db = new DBconnect();
const sync = new ConnectionInfo();

const {
    pushRoute,
    popRoute,
} = actions;

const { width } = Dimensions.get('window');

class Report extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: 'Report',
      showDate: true,
      startDate: new Date().toJSON().slice(0,10).replace(/-/g,'-'),
      endDate: new Date().toJSON().slice(0,10).replace(/-/g,'-'),
      transactionDate: '',
      loading: undefined,
    };
  }

  routeNavigate(route) {
    if (!route) {
      this.props.popRoute(this.props.navigation.key);
    } else {
      this.props.pushRoute({ key: route, index: 1 }, this.props.navigation.key);
    }
  }

  routeTo(args) {
      this.routeNavigate(args)
  }

  async getTransaction() {
    this.setState({ loading: true });
    const query = await db.query(`select cd_transaction_customer.*, cd_customer.Name from cd_transaction_customer LEFT join cd_customer ON cd_transaction_customer.CustomerID = cd_customer.FarmerID WHERE cd_transaction_customer.DateCreated BETWEEN "${this.state.startDate}" AND "${this.state.endDate}";`);
    this.setState({ loading: false });
    return query;
  }

  async incomeWow() {
    this.setState({ loading: true });
    const query = await db.query(`SELECT * FROM wow_income WHERE TanggalPenjualan BETWEEN "${this.state.startDate}" AND "${this.state.endDate}";`);
    this.setState({ loading: false });
    return query;
  }

  async outcomeWow() {
    this.setState({ loading: true });
    const query = await db.query(`SELECT * FROM wow_outcome WHERE Tanggal BETWEEN "${this.state.startDate}" AND "${this.state.endDate}";`);
    this.setState({ loading: false });
    return query;
  }

  async outcome() {
    this.setState({ loading: true });
    const query = await db.query(`SELECT * FROM cvc_inoutcome WHERE Kind = 2 AND Tanggal BETWEEN "${this.state.startDate}" AND "${this.state.endDate}";`)
    this.setState({ loading: false });
    return query;
  }

  async searchReport() {
    const transactionDate = (this.state.endDate === this.state.startDate) ? this.state.startDate : `${this.state.startDate} to ${this.state.endDate}`;
    this.setState({
      showDate: false,
      loading: true,
      transactionDate,
    });
  }

  async getTotalTransaction() {
    const query = await db.query(`select sum(Total) as income from cd_transaction_customer WHERE DateCreated BETWEEN "${this.state.startDate}" AND "${this.state.endDate}";`);
    return query;    
  }

  async getTotalIncomeWow() {
    const query = await db.query(`select sum(Total) as wowincome from wow_income WHERE TanggalPenjualan BETWEEN "${this.state.startDate}" AND "${this.state.endDate}";`);
    return query;
  }

  async getTotalOutcomeWow() {
    const query = await db.query(`select sum(Total) as wowoutcome from wow_outcome WHERE Tanggal BETWEEN "${this.state.startDate}" AND "${this.state.endDate}";`);
    return query;
  }

  async getTotalOutcome() {
    const query = await db.query(`SELECT sum(Total) as cvcoutcome FROM cvc_inoutcome WHERE Kind = 2 AND Tanggal BETWEEN "${this.state.startDate}" AND "${this.state.endDate}";`);
    return query;
  }


  async getProfitLoss() {
    let Transaction = await this.getTotalTransaction();
    let IncomeWow = await this.getTotalIncomeWow();
    let OutcomeWow = await this.getTotalOutcomeWow();
    let Outcome = await this.getTotalOutcome();
    this.setState({ loading: false });
    return {
      Transaction: Transaction.item(0).income,
      IncomeWow: IncomeWow.item(0).wowincome,
      OutcomeWow: OutcomeWow.item(0).wowoutcome,
      Outcome: Outcome.item(0).cvcoutcome,
    };
  }

  render() {
    return (
      <Container theme={theme} style={{ backgroundColor: '#fff' }}>
        <Header style={{ elevation: 0, backgroundColor: '#8a6d4f' }}>
          <Left>
            <Button transparent onPress={this.props.openDrawer}>
              <Icon active name="menu" />
            </Button>
          </Left>
          <Body>
            <Title><Icon name="md-paper" /> {this.state.title}</Title>
          </Body>
          <Right>
            <Icon name="md-refresh" onPress={() => this.setState({ showDate: true })} />
          </Right>
        </Header>
        
        {
          (!this.state.showDate) ?
          
          <ScrollableTabView
              tabBarInactiveTextColor="#ccc"
              tabBarActiveTextColor="#fff"
              initialPage={0}
              renderTabBar={() => <ScrollableTabBar style={{ backgroundColor: '#8a6d4f' }} />}
            >
              <ScrollView tabLabel={$t('Profit & Loss')}>
                { this.state.loading  && <Spinner color="#654141" />}
                <ProfitLoss dataProfitLoss={this.getProfitLoss.bind(this)} transactionDate={this.state.transactionDate}/>
              </ScrollView>
              <View tabLabel={$t('Sales Transaction')} style={styles.tabView}>
                { this.state.loading  && <Spinner color="#654141" />}
                <TransactionIncome transactionData={this.getTransaction.bind(this)} transactionDate={this.state.transactionDate}/>
              </View>
              <View tabLabel={$t('WowFarm Income')} style={styles.tabView}>
                { this.state.loading  && <Spinner color="#654141" />}
                <IncomeWow incomeData={this.incomeWow.bind(this)} transactionDate={this.state.transactionDate}/>
              </View>
              <View tabLabel={$t('Sales Expense')} style={styles.tabView}>
              { this.state.loading  && <Spinner color="#654141" />}
                <Expense outcome={this.outcome.bind(this)} transactionDate={this.state.transactionDate}/>
              </View>
              <View tabLabel={$t('WowFarm Expense')} style={styles.tabView}>
              { this.state.loading  && <Spinner color="#654141" />}
                <ExpenseWow outcomeData={this.outcomeWow.bind(this)} transactionDate={this.state.transactionDate}/>
              </View>
            </ScrollableTabView>
          :
          <Content>
            
          <Card>
            <CardItem>
              <Content>
                <Grid>
                  <Row>
                    <Col>
                      <DatePicker
                      style={{ width: width / 2.5 + 20 }}
                      date={this.state.startDate}
                      mode="date"
                      placeholder="Date From"
                      format="YYYY-MM-DD"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      showIcon
                      onDateChange={
                            (date) => {
                              this.setState({ startDate: date });
                            }
                        }
                    />
                    </Col>
                    <Col>
                      <DatePicker
                      style={{ width: width / 2.5 + 20 }}
                      date={this.state.endDate}
                      mode="date"
                      placeholder="Date To"
                      format="YYYY-MM-DD"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      showIcon
                      onDateChange={
                            (date) => { this.setState({ endDate: date }) ;}
                        }
                    />
                    </Col>
                  </Row>
                  <Row style={{ marginTop: 10 }}>
                    <Col>
                      <Button warning block onPress={() => this.searchReport() }>
                          <Icon name="md-search" style={{color:'#fff'}}/>
                          <Text> {$t('Search')} </Text>
                      </Button >
                    </Col>
                  </Row>
                </Grid>
              </Content>
            </CardItem>
          </Card>
        </Content>
        }
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    pushRoute: (route, key) => dispatch(pushRoute(route, key)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(Report);
