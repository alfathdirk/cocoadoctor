
import React, { Component } from 'react';
import { Image, Platform, ScrollView, ToastAndroid, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { actions } from 'react-native-navigation-redux-helpers';
import { Container, Content, Text, Item, Input, Button, Icon, View, Label } from 'native-base';
import SplashScreen from 'react-native-splash-screen'

import styles from './styles';
import DBconnect from '../../database/db';

import PushData from '../../actions/pushdata';

const db = new DBconnect('batch');

const {
  pushRoute,
  replaceAt,
} = actions;

const backgroundImage = require('../../../images/glow2.png');
const logo = require('../../../images/logo.png');

class Login extends Component {

  static propTypes = {
    replaceAt: React.PropTypes.func,
    pushRoute: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      scroll: false,
    };
  }

  async componentWillMount() {
    SplashScreen.hide()
    global.serverUri = 'https://demo.cocoatrace.com';
    // global.serverUri = 'http://192.168.1.99';
    await db.table('ktv_setting').findAll().then((res) => {
      if(res.length > 0){
        global.serverUri = res.item(0).serverUri;
      }
    });

  }

  replaceAt(route) {
    this.props.replaceAt('login', { key: route }, this.props.navigation.key);
  }

  pushRoute(route) {
    console.log(this.props.navigation.key);
    this.props.pushRoute({ key: route, index: 1 }, this.props.navigation.key);
  }

  async getLogin() {
    let email = this.state.email;
    let password = this.state.password;
    const body = { username: email, passwd: password, traceability: 'true'};
    if((email !== '') && (password !== '')) {
      let results = await new PushData().endpoint('auth-traceability/login').data(body).post();
      if(results.success === true) {
        await db.query('DROP TABLE IF EXISTS "ktv_mobile_profile"');
        await db.query('CREATE TABLE IF NOT EXISTS "ktv_mobile_profile" ( "ProfileMAC" TEXT NOT NULL, "ProfileName" TEXT NOT NULL, "ProfileValue" TEXT )');
        for (let key in results.results) {
          if (results.results.hasOwnProperty(key)) {
            let obj = {
              ProfileMAC: key,
              ProfileName: 'undefined',
              ProfileValue: results.results[key],
            }
            await db.table('ktv_mobile_profile').save({ key: undefined }, obj);
          }
        }
          ToastAndroid.showWithGravity(
            `Login Successfully`,
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
          );

          this.replaceAt('home');
      } else {
          ToastAndroid.showWithGravity(
            `Login Failed Check Username or Password`,
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
          );
      }
    } else {
      ToastAndroid.showWithGravity(
        `Please input Email and Password`,
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
    }
  }

  render() {
    return (
      <Container>
        <Content keyboardShouldPersistTaps='always' style={{ backgroundColor: '#b3bfb4' }} scrollEnabled={this.state.scroll}>
            <Image source={backgroundImage} style={styles.container}>
              <Image source={logo} style={styles.shadow}>
                <View style={styles.bg}>
                      <Item floatingLabel underline style={{ marginBottom: 10 }}>
                        <Label style={{ left: 10, color: '#fff'}}>
                          Email
                        </Label>
                        <Icon active name="person" />
                        <Input
                          placeholderTextColor="#FFF"
                          onChangeText={email => this.setState({ email })}
                        />
                      </Item>
                      <Item floatingLabel underline style={{ marginBottom: 30 }}>
                        <Label style={{ left: 10, color: '#fff'}}>
                          Password
                        </Label>
                        <Icon name="unlock" />

                        <Input
                          placeholderTextColor="#FFF"
                          secureTextEntry
                          onChangeText={password => this.setState({ password })}
                          onSubmitEditing={ () => this.getLogin() }
                        />
                      </Item>
                  <Button transparent style={{ alignSelf: 'flex-end', marginBottom: (Platform.OS === 'ios') ? 5 : 0, marginTop: (Platform.OS === 'ios') ? -10 : 0 }}>
                    <Text>
                      Lupa Password ?
                    </Text>
                  </Button>
                  <Button keyboardShouldPersistTaps='always' block style={{ marginBottom: 20 }} onPress={() => this.getLogin()}>
                    <Text style={{ color:'green' }}>
                      Login
                    </Text>
                  </Button>
                </View>
              </Image>
            </Image>
        </Content>
      </Container>
    );
  }
}


function bindActions(dispatch) {
  return {
    replaceAt: (routeKey, route, key) => dispatch(replaceAt(routeKey, route, key)),
    pushRoute: (route, key) => dispatch(pushRoute(route, key)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindActions)(Login);
