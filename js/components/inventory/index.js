import React, { Component } from 'react';
import { Dimensions, ListView, View, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';

import { SwipeListView } from 'react-native-swipe-list-view';
import ActionButton from 'react-native-action-button';

import { Col, Row, Grid } from 'react-native-easy-grid';

import { actions } from 'react-native-navigation-redux-helpers';
import { Container, Badge, Header, Title, Content, Text, Thumbnail, Footer, Button, Icon, Left, Body, Right, Form, Label, Item, Input, Card, CardItem } from 'native-base';
import ModalForm from './form';

import { openDrawer } from '../../actions/drawer';
import theme from '../../themes/base-theme';

import DBconnect from '../../database/db';

import ConnectionInfo from '../../helpers/connection';
import styles from './styles';


import iconTransaction from '../../../images/icon/synced.png';
import iconKarung from '../../../images/icon/karung.png';
import Translate from '../../helpers/translation';

const $t = new Translate().t;

const db = new DBconnect();
const sync = new ConnectionInfo();

const {
  pushRoute,
  popRoute,
} = actions;

const { width } = Dimensions.get('window');

class Inventory extends Component {

    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
          inventories: [],
          showModal: false,
          CurrentInventoryID: '',
        };
    }

    async componentDidMount() {
        let invetory = [];
        let data = await db.table('cd_inventory').findAll({ StatusRevision: '' });
        for (var i = 0; i < data.length; i++) {
          invetory.push(data.item(i));
        }
        this.setState({
            inventories: invetory
        })
    }

    setStateFrag(args) {
        this.setState(args);
        if(typeof args.refresh !== 'undefined') {
            this.componentDidMount();
        }
    }

    editInvetory(item) {
        this.setState({ CurrentInventoryID: item.id, showModal: true });
    }

    deleteInventory(item) {
      db.table('cd_inventory').save({ id: item.id }, { StatusRevision: 'Deleted', synced: 0 });
      this.componentDidMount();
      sync.dbInventory();
    }

    get listBarang() {
        return(
            <View>
            <SwipeListView
                dataSource={this.ds.cloneWithRows(this.state.inventories)}
                enableEmptySections={true}
                disableRightSwipe={true}
                renderRow={ item => (
                <TouchableHighlight onPress={ () => {  this.editInvetory(item) }}>
                    <View style={styles.rowFront}>
                    <Grid>
                    <Col>
                        {item.synced == 1 ?  
                        <Icon name="md-cloud-done" style={{ color: '#3e9ab5',fontSize: 40}}/>
                        :
                        <Icon name="ios-cloud-outline" style={{ color: '#3e9ab5', fontSize: 40}}/>
                        }
                        </Col>
                    <Col size={5}>
                        <Text style={{ color: '#4d5259' }}> { item.Name }</Text>
                        <Text style={{ color: '#4d5259' }}>Stock: {item.Stock} </Text>
                    </Col>
                    <Col size={2} style={{ justifyContent: 'center' }}>
                        <Badge info>
                        <Text style={{ }}>Rp: {item.Price} </Text>
                        </Badge>
                    </Col>
                    </Grid>
                    </View>
                    </TouchableHighlight>
                )}
                renderHiddenRow={ (data, secId, rowId, rowMap) => (
                <View style={styles.rowBack}>
                    <Thumbnail square style={{width: 40, height: 40}}
                    source={iconKarung}
                    />
                <Button danger style={{ height:60 }} onPress={() => { this.deleteInventory(data) }}>
                    <Icon name="md-trash"/>
                    </Button>
                </View>
                )}
                leftOpenValue={75}
                rightOpenValue={-57}
            />
            </View> 
        )
    }

    render() {
        return(
            <Container theme={theme} style={{ backgroundColor: '#fff' }}>
                <Header style={{ elevation: 0, backgroundColor: '#8a6d4f' }}>
                    <Left>
                        <Button transparent onPress={this.props.openDrawer}>
                            <Icon active name="md-menu" />
                        </Button>
                    </Left>
                    <Body>
                        <Title><Icon name="md-albums" /> {$t('Products')}</Title>
                    </Body>
                    <Right/>
                </Header>
                <ModalForm showModal={this.state.showModal} setParentState={this.setStateFrag.bind(this)} inventoryID={this.state.CurrentInventoryID}/>
                {this.listBarang}
                <ActionButton buttonColor="#8a6d4f" style={styles.actionButton} onPress={() => { this.setState({showModal: true, CurrentInventoryID: '' }) }}></ActionButton>
            </Container>
        )
    }
}
function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    popRoute: key => dispatch(popRoute(key)),
    pushRoute: (route, key) => dispatch(pushRoute(route, key)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(Inventory);
