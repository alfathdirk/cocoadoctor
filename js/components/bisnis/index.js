import React, { Component } from 'react';
import { Alert, TextInput, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { actions } from 'react-native-navigation-redux-helpers';
import TextField from 'react-native-md-textinput';
import DatePicker from 'react-native-datepicker';
import ActionButton from 'react-native-action-button';

import { Container, Thumbnail, Header, Title, Content, Text, Tabs, Image, Tab, Footer, Button, Icon, Left, Body, Right, Form, Label, Item, Input, Card, CardItem } from 'native-base';
import theme from '../../themes/base-theme';
import styles from './styles';
// import PushData from '../../actions/pushdata';
import DBconnect from '../../database/db';

import Format from '../../helpers/Format';
import ConnectionInfo from '../../helpers/connection';

import { openDrawer } from '../../actions/drawer';

import ListWowfarm from './wowfarm';
import ListBusiness from './business';
import Translate from '../../helpers/translation';

const $t = new Translate().t;

const db = new DBconnect();
const sync = new ConnectionInfo();

const {
    pushRoute,
    popRoute,
} = actions;

const { width } = Dimensions.get('window');


class Business extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: 'Bisnis',
        }
    }

    directPageOutcome(type) {
        global.bisniscvcID = '';
        global.wowOutComeID = '';
        global.cvcOutcome = 2;
        let route = 'bisnis-outcome-form';
        if(type == 'wow') {
            return this.routeNavigate(route)
        } 
        return this.routeNavigate('form-bisnis-cvc-income')
    }

    directPageIncome(to) {
        let route = 'form-bisnis-cvc-income';
        global.bisniscvcID = '';
        global.wowIncomeID = '';
        global.cvcOutcome = 1;
        if(to !== 'bisnis') {
            route = 'wow-farm-income';
        }
        this.routeNavigate(route)
    }

    
    routeNavigate(route) {
        if (!route) {
            this.props.popRoute(this.props.navigation.key);
        } else {
            this.props.pushRoute({ key: route, index: 1 }, this.props.navigation.key);
        }
    }


    actionButton(type) {
        return(
        <ActionButton buttonColor="#8a6d4f">
            <ActionButton.Item buttonColor='#b99d77' title={$t('Expense')} onPress={() => {
                this.directPageOutcome(type);
            }}>
                <Icon name="md-cart" style={styles.actionButtonIcon} />
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#7b8e65' title={$t('Income')} onPress={() => {
                this.directPageIncome(type);
            }}>
                <Icon name="md-basket" style={styles.actionButtonIcon} />
            </ActionButton.Item>
        </ActionButton>    
        )
    }
    
    routeTo(args) {
        this.routeNavigate(args)
    }

    render() {
        return(
            <Container theme={theme} style={{ backgroundColor: '#fff' }}>
            <Header hasTabs style={{ elevation: 0, backgroundColor: '#8a6d4f' }}>
              <Left>
                <Button transparent onPress={this.props.openDrawer}>
                  <Icon active name="menu" />
                </Button>
              </Left>
              <Body>
                <Title><Icon name="md-paper" /> {this.state.title}</Title>
              </Body>
              <Right />
            </Header>
            <Tabs>
                <Tab heading="Sales" >
                    <ListBusiness route={this.routeTo.bind(this)} />
                    { this.actionButton('bisnis')}

                </Tab>
                <Tab heading={$t('Wow Farm')} >
                    <ListWowfarm route={this.routeTo.bind(this)}/>
                    { this.actionButton('wow')}
                </Tab>
            </Tabs>
            </Container>
        )
    }
}

function bindAction(dispatch) {
    return {
        openDrawer: () => dispatch(openDrawer()),
        pushRoute: (route, key) => dispatch(pushRoute(route, key)),
    };
  }
  
const mapStateToProps = state => ({
navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(Business);
