import React, { Component } from 'react';
import { BackAndroid, StatusBar, NavigationExperimental, Text } from 'react-native';
import { connect } from 'react-redux';
import { Drawer } from 'native-base';
import { actions } from 'react-native-navigation-redux-helpers';
import { closeDrawer } from './actions/drawer';
import { statusBarColor } from './themes/base-theme';
import Login from './components/login/';
import Home from './components/home/';
import CTSideBar from './components/ct-side-bar';
import Customer from './components/customer/';
import ListFarmer from './components/farmer/';
import Inventory from './components/inventory/';
import Profile from './components/profile/';
import CTAccount from './components/ct-account/';
import Business from './components/bisnis/';
import BusinessFormIncome from './components/bisnis/FormIncome/businessForm';
import WowFormIncome from './components/bisnis/FormIncome/wowForm';
import OutcomeForm from './components/bisnis/FormOutcome/outcomeForm';
import Coaching from './components/coaching/';
import TransactionCustomer from './components/transactionCostumer/form';
import Report from './components/report/';

import CTSync from './components/ct-synchronization';
import DBconnect from './database/db';
import ConnectionInfo from './helpers/connection';
import Translate from './helpers/translation';

const $t = new Translate().t;
const db = new DBconnect('batch');

const {
  popRoute,
} = actions;

const {
  CardStack: NavigationCardStack,
} = NavigationExperimental;

class AppNavigator extends Component {

  // static propTypes = {
  //   drawerState: React.PropTypes.string,
  //   popRoute: React.PropTypes.func,
  //   closeDrawer: React.PropTypes.func,
  //   navigation: React.PropTypes.shape({
  //     key: React.PropTypes.string,
  //     routes: React.PropTypes.array,
  //   }),
  // }

  constructor(props) {
    super(props);
    global.userSession = '';
  }

  componentWillMount() {
    db.table('ktv_mobile_profile').findAll().then((result) => {
      if(result.length > 0){
        global.userSession = result.item(0).ProfileValue;
      }
    });
  }

  componentDidMount() {
    BackAndroid.addEventListener('hardwareBackPress', () => {
      const routes = this.props.navigation.routes;

      if (routes[routes.length - 1].key === 'home' || routes[routes.length - 1].key === 'login') {
        return false;
      }

      this.props.popRoute(this.props.navigation.key);
      return true;
    });
  }

  componentDidUpdate() {
    if (this.props.drawerState === 'opened') {
      this.openDrawer();
    }

    if (this.props.drawerState === 'closed') {
      this._drawer._root.close();
    }
  }

  popRoute() {
    this.props.popRoute();
  }

  openDrawer() {
    this._drawer._root.open();
  }

  closeDrawer() {
    if (this.props.drawerState === 'opened') {
      this.props.closeDrawer();
    }
  }

  t(words) {
    return $t(words);
  }

  setStateFrag(args) {
    console.log(args)
  }

  _renderScene(props) { // eslint-disable-line class-methods-use-this
    switch (props.scene.route.key) {
      case 'home':
        return <Home />;
      case 'profile':
        return <Profile />;
      case 'farmer':
        return <ListFarmer />;
      case 'inventory':
        return <Inventory />;
      case 'business':
        return <Business />;
      case 'report':
        return <Report />;
      case 'form-bisnis-cvc-income':
        return <BusinessFormIncome />;
      case 'wow-farm-income':
        return <WowFormIncome />;
      case 'bisnis-outcome-form':
        return <OutcomeForm />;
      case 'coaching':
        return <Coaching />;
      case 'transactionCustomer':
        return <TransactionCustomer />;
      case 'ct-synchronization':
        return <CTSync />;
      case 'ct-account':
        return <CTAccount />;
      case 'customer-menu':
        return <Customer />;
      default :
        if(global.userSession != '') {
          return <Home />
        } else {
          return <Login />
        }
        break;
    }
  }

  render() {
    return (
      <Drawer
        ref={(ref) => { this._drawer = ref; }}
        type="displace"
        tweenDuration={150}
        content={<CTSideBar lang={this.t.bind(this)} />}
        tapToClose
        acceptPan={false}
        onClose={() => this.closeDrawer()}
        openDrawerOffset={0.5}
        panCloseMask={0.5}
        styles={{
          drawer: {
            shadowColor: '#000000',
            shadowOpacity: 0.8,
            shadowRadius: 3,
          },
        }}
        tweenHandler={(ratio) => {  //eslint-disable-line
          return {
            drawer: { shadowRadius: ratio < 0.2 ? ratio * 5 * 5 : 5 },
            main: {
              opacity: (2 - ratio) / 2,
            },
          };
        }}
        negotiatePan
      >
         <ConnectionInfo status={this.setStateFrag.bind(this)}/>
        <StatusBar
          backgroundColor={statusBarColor}
          barStyle="light-content"
        />
        <NavigationCardStack
          navigationState={this.props.navigation}
          renderOverlay={this._renderOverlay}
          renderScene={this._renderScene}
        />
      </Drawer>
    );
  }
}

function bindAction(dispatch) {
  return {
    closeDrawer: () => dispatch(closeDrawer()),
    popRoute: key => dispatch(popRoute(key)),
  };
}

const mapStateToProps = state => ({
  drawerState: state.drawer.drawerState,
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(AppNavigator);
