import React, { Component } from 'react';
import { Modal, Alert, ToastAndroid } from 'react-native';
import { Container, Body, Left, Footer, Title, Right, Content, Text, Button, View, Form, Item, Label, Input, Header, Icon } from 'native-base';
import { Row, Col, Grid } from 'react-native-easy-grid';
import { Buffer } from 'buffer'

import TextField from 'react-native-md-textinput';

import DBconnect from '../../database/db';

const db = new DBconnect('batch');
global.Buffer = Buffer;

export default class ModalSave extends Component {

  constructor(props) {
    super(props)
    this.state = {
      visible: this.props.visible,
      username: global.name,
      password: '',
    }
  }

  setParentState(props){
    this.props.setParentState(props);
  }


    async save() {
      let server = this.props.server;
      let email = this.state.username;
      let password = this.state.password;
      const body = { username: email, passwd: password, traceability: 'true'};

      if((email !== '') && (password !== '')) {
        const head = {
          method: 'POST',
          credentials: 'include',
          headers: {
            'Content-Type': 'application/json',
            'User-Agent': 'mobile crosswalk',
          },
          body: JSON.stringify(body),
        };
        let uri = server+'/api/index.php/auth-traceability/login';
        try {
          let response = await fetch(uri,head);
          let responseJson = await response.json();
          let results = responseJson.results;
          if(typeof results !== 'undefined') {
            await db.query('DROP TABLE IF EXISTS "ktv_mobile_profile"');
            await db.query('CREATE TABLE IF NOT EXISTS "ktv_mobile_profile" ( "ProfileMAC" TEXT NOT NULL, "ProfileName" TEXT NOT NULL, "ProfileValue" TEXT )');
            for (var key in results) {
              if (results.hasOwnProperty(key)) {
                let obj = {
                  ProfileMAC: key,
                  ProfileName: 'undefined',
                  ProfileValue: results[key],
                }
                await db.table('ktv_mobile_profile').save({ key: undefined }, obj);
              }
            }
            await db.query('DELETE FROM ktv_setting');
            await db.table('ktv_setting').save({ key: undefined }, { serverUri: server });
            global.serverUri = server;
            ToastAndroid.showWithGravity(
              `Save Successfully`,
              ToastAndroid.SHORT,
              ToastAndroid.CENTER,
            );
            this.setParentState({showDialogForm: false})
          } else {
            ToastAndroid.showWithGravity(
              `Login Failed Check Username or Password`,
              ToastAndroid.SHORT,
              ToastAndroid.CENTER,
            );
          }
        } catch(err) {
          ToastAndroid.showWithGravity(
            `Error ${err}`,
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
          );
        }
      } else {
        ToastAndroid.showWithGravity(
          `Please input Email and Password`,
          ToastAndroid.SHORT,
          ToastAndroid.CENTER,
        );
      }
    }

  render() {
    return(
    <Modal
      transparent={true}
      visible={this.props.visible}
      onRequestClose={() => {this.setState({visible: false})}}>
      <View style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center'}}>
          <View style={{
              width: 400,
              height: 300}}>
              <Container style={{ backgroundColor: '#fafafa' }}>
                  <Header style={{ elevation: 0, backgroundColor: '#4c594f' }}>
                    <Left/>
                    <Body>
                      <Title><Icon name="md-print" /> Login</Title>
                    </Body>
                    <Right />
                  </Header>
                  <Content padder style={{ backgroundColor: 'transparent' }}>
                      <TextField
                          label={'Username'}
                          highlightColor={'#45be5c'}
                          dense={true}
                          value={this.state.username}
                          onChangeText={(text) => {
                            this.setState({username:text});
                          }}
                      />

                      <TextField
                          label={'Password'}
                          highlightColor={'#45be5c'}
                          dense={true}
                          secureTextEntry
                          value={this.state.password}
                          onChangeText={(text) => this.setState({password:text})}
                      />
                  </Content>
                  <Footer style={{ height: 80, backgroundColor: '#8c8c85' }}>
                    <Grid>
                      <Row>
                        <Col>
                          <Button block success style={{margin: 15}} onPress={() => this.save()}>
                            <Icon active name="ios-add-circle" style={{ fontSize: 30, lineHeight: 32 }} />
                            <Text>Save</Text>
                          </Button>
                        </Col>
                        <Col>
                          <Button block danger style={{margin: 15}} onPress={() => this.setParentState({showDialogForm: false})}>
                            <Icon active name="md-close-circle" style={{ fontSize: 30, lineHeight: 32 }} />
                            <Text>Cancel</Text>
                          </Button>
                        </Col>
                      </Row>
                    </Grid>
                  </Footer>
                </Container>
            </View>
          </View>
    </Modal>
    )
  }
}
