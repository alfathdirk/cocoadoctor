import { StyleSheet } from 'react-native';

export default {
  mb25: {
    marginBottom: 25,
  },
  label: {
    color: '#9E9E9E',
  },
  colum: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'stretch',
  },
  columchild: {
    flex: 1,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  text: {
    color: 'black',
  },
  autocomplete: {
    alignSelf: 'stretch',
    height: 50,
    backgroundColor: '#FFF',
    borderColor: 'lightblue',
    borderWidth: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    marginBottom: 10,
    marginTop: 50,
  },
};
