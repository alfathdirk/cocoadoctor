import React, { Component } from 'react';
import BarcodeScanner from 'react-native-barcodescanner';

export default class BarcodeScannerApp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      torchMode: 'off',
      cameraType: 'back',
    };
  }

  setParentState(args){
    this.props.setParentState(args);
  }

  barcodeReceived(e) {
    if(e.data){
      this.setParentState({ showCamera: false, e });
    }
  }

  render() {
    if(this.props.showCamera === true){
      return (
        <BarcodeScanner
          onBarCodeRead={this.barcodeReceived.bind(this)}
          style={{ flex: 1 }}
          torchMode={this.state.torchMode}
          cameraType={this.state.cameraType}
        />
      );
    } else {
      return(false);
    }
  }
}
