import React, { Component } from 'react';
import { Alert, TextInput, Dimensions, ScrollView } from 'react-native';

import { Col, Row, Grid } from 'react-native-easy-grid';
import TextField from 'react-native-md-textinput';

import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';

import { Container, Thumbnail, Header, View, Title, Content, Text, Tabs, Image, Tab, Footer, Button, Icon, Left, Body, Right, Form, Label, Item, Input, Card, CardItem } from 'native-base';
import theme from '../../themes/base-theme';
import styles from './styles';
import Format from '../../helpers/Format';

import DBconnect from '../../database/db';
import Translate from '../../helpers/translation';

const $t = new Translate().t;

const db = new DBconnect();

const { width } = Dimensions.get('window');

export default class Expense extends Component {

  constructor(props) {
    super(props);
    this.state = {
      anu: 'asdf',
      totalAmount: 0,
      dataTrans: [],
    };
  }

  async componentWillMount() {
    let outcome = await this.props.outcome();
    let items = [];

    let totalAmount = 0;
    for (let i = 0; i < outcome.length; i++) {
      let item = outcome.item(i);
      totalAmount += parseInt(item.Total);
      items.push(item);
    }

    this.setState({
      totalAmount,
      dataTrans: items,
    });
  }

  render() {
    return(
      <View>
        <View style={{ height: 105 }}>
            <Grid style={{ borderBottomWidth: 0.3, borderBottomColor: '#ccc' }}>
              <Row>
                <Col style={{ justifyContent: 'center', flex: 0.2 }}>
                  <Icon style={{ color: '#795a5a', left: 10 }} active name="md-calendar" />
                </Col>
                <Col style={{ flex: 0.4, justifyContent: 'center' }}>
                  <Text style={{ color: 'black', fontWeight: 'bold', fontStyle: 'italic' }}>{$t('Date Range')}</Text>
                </Col>
                <Col style={{justifyContent: 'center'}}>
                  <Text style={{ color: 'black', textAlign: 'right', marginRight: 20, fontWeight: 'bold', fontStyle: 'italic', fontSize: 12 }}>{this.props.transactionDate}</Text>
                </Col>
              </Row>
              <Row style={{ borderBottomWidth: 1 , borderBottomColor: '#ccc' }}>
                <Col style={{ justifyContent: 'center', flex: 0.3 }}>
                  <Icon style={{ color: '#5a7b5b', left: 10 }} active name="md-cash" />
                </Col>
                <Col style={{justifyContent: 'center'}}>
                  <Text style={{ color: '#000', fontWeight: 'bold', fontStyle: 'italic' }}>{$t('Total Amount')}</Text>
                </Col>
                <Col style={{justifyContent: 'center'}}>
                  <Text style={{ color: '#000', textAlign: 'right', right:10 , fontStyle: 'italic', fontWeight: 'bold' }}>{Format.formatNumber(this.state.totalAmount)}</Text>
                </Col>
              </Row>

              <Row style={{ backgroundColor: '#a08467', paddingTop: 10 }}>
                <Col style={{ justifyContent: 'center' }}>
                  <Icon style={{ color: '#fff', textAlign: 'center' }} active name="md-calendar" />
                </Col>
                <Col style={{ justifyContent: 'center' }}>
                  <Icon style={{ color: '#fff', textAlign: 'center' }} active name="md-cash" />
                </Col>
              </Row>
            </Grid>
        </View>
        <ScrollView>
          <Content>
            <Grid>
              { this.state.dataTrans.map((v,k) => {
                return (
                  <Row style={{ borderBottomWidth: 1 , borderBottomColor: '#ccc', height: 35 }}>
                    <Col style={{ justifyContent: 'center', backgroundColor: (k % 2 == 0) ? '#f9f3ec' : 'white' }}>
                      <Text style={{color: 'black' , left: 10}}>
                        {v.Tanggal}
                      </Text>
                    </Col>
                    <Col style={{ justifyContent: 'center',  backgroundColor: (k % 2 == 0) ? '#f9f3ec' : 'white'  }}>
                      <Text style={{color: 'black', textAlign: 'right', right: 10 }}>
                        {Format.formatNumber(v.Total)}
                      </Text>
                    </Col>
                  </Row>
                );
              })}
            </Grid>
          </Content>
        </ScrollView>
      </View>
    );
  }
}
