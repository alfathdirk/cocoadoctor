import React, { Component } from 'react';
import { Alert, TextInput, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { actions } from 'react-native-navigation-redux-helpers';
import TextField from 'react-native-md-textinput';
import DatePicker from 'react-native-datepicker';

import { Container, Thumbnail, Header, Title, Content, Text, Tabs, Image, Tab, Footer, Button, Icon, Left, Body, Right, Form, Label, Item, Input, Card, CardItem } from 'native-base';
import theme from '../../themes/base-theme';
import styles from './styles';
// import PushData from '../../actions/pushdata';
import DBconnect from '../../database/db';

import Format from '../../helpers/Format';
import ConnectionInfo from '../../helpers/connection';
import Translate from '../../helpers/translation';

const $t = new Translate().t;

const db = new DBconnect();
const sync = new ConnectionInfo();

const {
    pushRoute,
    popRoute,
} = actions;

const { width } = Dimensions.get('window');

class Coaching extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: global.coachingID,
            FarmerID: '',
            FarmerName : '',
            tanggalCoaching: new Date().toJSON().slice(0,10).replace(/-/g,'-'),
            luasLahan: '',
            clones: '',
            notes: '',
            cvcImplement: '',
            issue: '',
            recomendation: '',
            dateend: new Date().toJSON().slice(0,10).replace(/-/g,'-'),
            CoachingSyncID: undefined,
        };
    }

    routeNavigate() {
        this.props.popRoute(this.props.navigation.key);
    }

    async componentWillMount() {

        if(global.FarmerID){
            let q = await db.table('cd_customer').findOne({ FarmerID: global.FarmerID });
            let { Name, FarmerID } = q.item(0);
            
            let nameAndId = {
                FarmerID: String(FarmerID),
                FarmerName: Name,
            };
            if(global.coachingID !== '') {
                let coaching = await db.table('cd_coaching').findOne({ id: global.coachingID });
                let {   notes, 
                        clones,
                        id, 
                        luasLahan, 
                        cvcImplement,
                        issue,
                        recomendation,
                        dateend,
                        CoachingSyncID, 
                    } = coaching.item(0);
                    
                let coachingData = {
                    notes,
                    clones,
                    id,
                    luasLahan,
                    cvcImplement,
                    issue,
                    recomendation,
                    dateend,
                    CoachingSyncID,
                }
                Object.assign(nameAndId, coachingData);
            }
            this.setState(nameAndId);
        }
        
    }
    get deleteButton() {
        if(global.coachingID !== '') {
        return(
            <Col>
                <Button block danger style={{margin: 15}} onPress={() =>  this.delete()}>
                <Icon active name="md-trash" style={{ fontSize: 30, lineHeight: 32 }} />
                <Text>Delete</Text>
                </Button>
            </Col>
        )
        } else {
            return(<Text/>);
        }
    }

    async save() {

        let lastNumberIDtrans = await Format.getLastID({ table: 'cd_coaching' , column: 'CoachingSyncID' }) 
        let CoachID = `${global.SceID}-${lastNumberIDtrans}`;
        let data = {
            CoachingSyncID: this.state.CoachingSyncID || CoachID,
            FarmerID: global.FarmerID,
            CustomerSyncID: global.CustomerSyncID,
            tanggalCoaching : this.state.tanggalCoaching,
            luasLahan : this.state.luasLahan,
            notes : this.state.notes,
            cvcImplement: this.state.cvcImplement,
            issue: this.state.issue,
            recomendation: this.state.recomendation,
            dateend: this.state.dateend,
            synced: 0,
            StatusRevision: '',

        };
        db.table('cd_coaching').save({ id: this.state.id }, data);
        this.routeNavigate();
        sync.dbCoaching();
    }

    delete() {
        Alert.alert(
            `Menghapus #${this.state.id}`,
            'Apakah anda yakin ?',
            [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'OK', onPress: () => {
                  db.table('cd_coaching').save({ id: this.state.id }, { StatusRevision: 'Deleted', synced: 0 });
                  this.routeNavigate();
                  sync.dbCoaching();
                    // db.query(`DELETE FROM cd_coaching WHERE id = ${this.state.id};`).then(() => {
                    // });
                }},
            ],
            { cancelable: false }
            )
    }

    render() {
        return(
            <Container theme={theme} style={{ backgroundColor: '#fff' }}>
                <Header hasTabs style={{ elevation: 0, backgroundColor: '#8a6d4f' }}>
                    <Left>
                        <Button transparent onPress={() => this.routeNavigate()}>
                            <Icon active name="ios-arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title><Icon name="md-create" />  Coaching</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content padder>
                    <Card>
              <CardItem header>
                <Icon name="md-book" style={{color:'#4a4949'}}/>
                <Text style={{color:'#4a4949'}}>Coaching Form</Text>
              </CardItem>
              <CardItem>
                      <Content>
                        <Grid>
                            <Row>
                                <Col style={{ flex: 0.1, justifyContent:'center' }} >
                                <Icon name="md-person" style={styles.iconForm} />
                                </Col>
                                <Col>
                                <TextField
                                    label={$t('Farmer Name')}
                                    highlightColor={'#45be5c'}
                                    dense={true}
                                    value={this.state.FarmerName}
                                    onChangeText={(text) => this.setState({FarmerName:text})}
                                />
                                </Col>
                            </Row>
                            <Row>
                            <Col style={{ flex: 0.1, justifyContent:'center' }} >
                              <Icon name="md-person" style={styles.iconForm} />
                            </Col>
                            <Col>
                              <TextField
                                  label={$t('Farmer ID')}
                                  highlightColor={'#45be5c'}
                                  dense={true}
                                  value={this.state.FarmerID}
                                  onChangeText={(text) => this.setState({FarmerID:text})}
                              />
                            </Col>
                            </Row>
                          <Row>
                            <Col style={{flex: 0.5, justifyContent: 'center'}}>
                              <Row>
                                  <Col style={{ flex: 0.3, justifyContent:'center' }} >
                                    <Icon name="md-calendar" style={styles.iconForm} />
                                  </Col>
                                  <Col style={{ justifyContent: 'center'}}>
                                    <Text style={{ color: '#7d7d7d', fontSize: 14, marginTop:10, marginLeft:10 }}>
                                      {$t('Date Coaching')}
                                    </Text>
                                  </Col>
                                </Row>
                            </Col>
                            <Col>
                              <DatePicker
                                  style={{width: 200,top:10}}
                                  date={this.state.tanggalCoaching}
                                  showIcon={false}
                                  mode="date"
                                  placeholder="select date"
                                  format="YYYY-MM-DD"
                                  confirmBtnText="Confirm"
                                  cancelBtnText="Cancel"
                                  customStyles={{
                                    dateIcon: {
                                      position: 'absolute',
                                      left: 0,
                                      top: 4,
                                      marginLeft: 0
                                    },
                                    dateInput: {
                                      marginLeft: 36
                                    }
                                  }}
                                  onDateChange={(date) => {this.setState({tanggalCoaching: date})}}
                                />
                            </Col>
                          </Row>

                        <Row>
                          <Col style={{ flex: 0.1, justifyContent:'center' }} >
                                <Icon name="md-paper" style={styles.iconForm} />
                          </Col>
                          
                          <Col>
                            <TextField
                                label={$t('Issue')}
                                dense={true}
                                highlightColor={'#45be5c'}
                                value={this.state.issue}
                                onChangeText={(text) => this.setState({issue:text})}
                            />
                          </Col>
                        </Row>

                        <Row>
                          <Col style={{ flex: 0.1}} >
                                <Icon name="md-book" style={styles.iconForm} />
                          </Col>
                          
                          <Col style={{top: 12}}>
                            <TextInput
                                multiline={true}
                                numberOfLines={4}
                                placeholder={$t('Note')}
                                onChangeText={(text) => this.setState({notes: text})}
                                style={{ borderColor:'#ccc',borderWidth: 1,textAlignVertical:'top'}}    
                                value={this.state.notes}/>
                          </Col>
                        </Row>

                        <Row>
                          <Col style={{ flex: 0.1, justifyContent:'center' }} >
                                <Icon name="md-globe" style={styles.iconForm} />
                          </Col>
                          
                          <Col>
                            <TextField
                                label={$t('CVC Implementation')}
                                dense={true}
                                highlightColor={'#45be5c'}
                                value={this.state.cvcImplement}
                                onChangeText={(text) => this.setState({cvcImplement:text})}
                            />
                          </Col>
                        </Row>

                        <Row>
                          <Col style={{ flex: 0.1 }} >
                                <Icon name="md-book" style={styles.iconForm} />
                          </Col>
                          
                          <Col style={{top: 15}}>
                            <TextInput
                                multiline={true}
                                numberOfLines={4}
                                placeholder={$t('Recomendation')}
                                onChangeText={(text) => this.setState({recomendation: text})}
                                style={{ borderColor:'#ccc',borderWidth: 1,textAlignVertical:'top'}}    
                                value={this.state.recomendation}/>
                          </Col>
                        </Row>

                        <Row style={{ top: 15,paddingBottom:18}}>
                            <Col style={{flex: 0.5, justifyContent: 'center'}}>
                              <Row>
                                  <Col style={{ flex: 0.3, justifyContent:'center' }} >
                                    <Icon name="md-calendar" style={styles.iconForm} />
                                  </Col>
                                  <Col style={{ justifyContent: 'center'}}>
                                    <Text style={{ color: '#7d7d7d', fontSize: 14, marginTop:10, marginLeft:10 }}>
                                      {$t('Date End')}
                                    </Text>
                                  </Col>
                                </Row>
                            </Col>
                            <Col>
                              <DatePicker
                                  style={{width: 200,top:10,paddingBottom:10}}
                                  date={this.state.dateend}
                                  showIcon={false}
                                  mode="date"
                                  placeholder="select date"
                                  format="YYYY-MM-DD"
                                  confirmBtnText="Confirm"
                                  cancelBtnText="Cancel"
                                  customStyles={{
                                    dateIcon: {
                                      position: 'absolute',
                                      left: 0,
                                      top: 4,
                                      marginLeft: 0
                                    },
                                    dateInput: {
                                      marginLeft: 36
                                    }
                                  }}
                                  onDateChange={(date) => {this.setState({dateend: date})}}
                                />
                            </Col>
                          </Row>
                        </Grid>
                      </Content>
                </CardItem>
            </Card>
                </Content>
                <Footer style={{ height: 70, backgroundColor: '#8c8c85' }} >
                <Grid>
                    <Col>
                        <Button block success style={{margin: 15}} onPress={() =>  this.save()}>
                        <Icon active name="md-create" style={{ fontSize: 30, lineHeight: 32 }} />
                        <Text>{$t('Save')}</Text>
                        </Button>
                    </Col>
                    { this.deleteButton }
                </Grid>
                </Footer>

          
            </Container>
        )
    }
}


function bindAction(dispatch) {
  return {
    popRoute: key => dispatch(popRoute(key)),
    pushRoute: (route, key) => dispatch(pushRoute(route, key)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(Coaching);
