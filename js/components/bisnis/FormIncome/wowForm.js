import React, { Component } from 'react';
import { View, ScrollView, StatusBar, Dimensions, Picker,  ListView, TextInput } from 'react-native';
import { connect } from 'react-redux';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { actions } from 'react-native-navigation-redux-helpers';
import TextField from 'react-native-md-textinput';
import DatePicker from 'react-native-datepicker';

import { Container, Thumbnail, Header, Title, Content, Text, Tabs, Image, Tab, Footer, Button, Icon, Left, Body, Right, Form, Label, Item, Input, Card, CardItem } from 'native-base';
import theme from '../../../themes/base-theme';
import styles from './../styles';
// import PushData from '../../actions/pushdata';
import DBconnect from '../../../database/db';
import ConnectionInfo from '../../../helpers/connection';
import Format from '../../../helpers/Format';

import Translate from '../../../helpers/translation';

const $t = new Translate().t;

const db = new DBconnect();
const sync = new ConnectionInfo();

const {
    pushRoute,
    popRoute,
} = actions;

const { width } = Dimensions.get('window');

class WowFormIncome extends Component {
    constructor(props) {
        super(props)
        this.state = {
            TanggalPanen:  new Date().toJSON().slice(0,10).replace(/-/g,'-'),
            HariKering: '',
            BijiKering: '',
            BijiBasah: '',
            TanggalPenjualan:  new Date().toJSON().slice(0,10).replace(/-/g,'-'),
            Harga: '',
            Total: '',
            Keterangan: '',
            Synced: '0',
        }
    }

    routeNavigate(route) {
        if (!route) {
            this.props.popRoute(this.props.navigation.key);
        } else {
            this.props.pushRoute({ key: route, index: 1 }, this.props.navigation.key);
        }
    }

    get deleteButton() {
        if(global.wowIncomeID !== '') {
        return(
            <Col>
                <Button block danger style={{margin: 15}} onPress={() =>  this.delete()}>
                <Icon active name="md-trash" style={{ fontSize: 30, lineHeight: 32 }} />
                <Text>Delete</Text>
                </Button>
            </Col>
        )
        } else {
            return(<Text/>);
        }
    }

    async delete() {
      await db.table('wow_income').save({ id: this.state.id },{ StatusRevision: 'Deleted', synced: 0 });
      sync.dbWowIn();
      this.routeNavigate();
    }

    async save() {
      let lastNumber = await Format.getLastID({ table: 'wow_income' , column: 'wowInSyncID' });
      if(global.wowIncomeID == '') {
        this.setState({
          wowInSyncID: `${global.SceID}-${lastNumber}`,
        });
      }
      // alert(JSON.stringify(this.state));

      this.setState({
        synced: 0,
        StatusRevision: '',
        Harga: this.state.Harga.replace(/\./g,''),
        Total: this.state.Total.replace(/\./g,''),
      }); 

      await db.table('wow_income').save({ id: global.wowIncomeID }, this.state);
      sync.dbWowIn();
      this.routeNavigate()
    }

    async componentDidMount() {
      if(global.wowIncomeID != '') {
        let dataIncome = await db.table('wow_income').findOne({ id: global.wowIncomeID });
        let dataObj = dataIncome.item(0)
        Object.assign(dataObj, { 
          Harga: Format.formatNumber(dataObj.Harga),
          Total: Format.formatNumber(dataObj.Total),
        });
        this.setState(dataObj);
      }
    }

    render() {
        return(
            <Container theme={theme} style={{ backgroundColor: '#fff' }}>
            <Header hasTabs style={{ elevation: 0, backgroundColor: '#8a6d4f' }}>
                <Left>
                    <Button transparent onPress={() => this.routeNavigate()}>
                        <Icon active name="ios-arrow-back" />
                    </Button>
                </Left>
                <Body style={{ flex: 2 }}>
                    <Title><Icon name="md-create" /> {$t('Wow Farm')}</Title>
                </Body>
            </Header>
            <Content padder>
                <Card>
          <CardItem header>
            <Icon name="md-person" style={{color:'#4a4949'}}/>
            <Text style={{color:'#4a4949'}}>{$t('Income')}</Text>
          </CardItem>
          <CardItem>
                  <Content>
                  <Grid>
                        <Row>
                            <Col style={{flex: 0.5, justifyContent: 'center'}}>
                            <Row>
                                <Col style={{ flex: 0.3, justifyContent:'center' }} >
                                    <Icon name="md-calendar" style={styles.iconForm} />
                                </Col>
                                <Col style={{ justifyContent: 'center'}}>
                                    <Text style={{ color: '#7d7d7d', fontSize: 14, marginTop:10, marginLeft:10 }}>
                                    {$t('Harvest Date')}
                                    </Text>
                                </Col>
                                </Row>
                            </Col>
                            <Col>
                                <DatePicker
                                style={{width: 200,top:10}}
                                date={this.state.TanggalPanen}
                                showIcon={false}
                                mode="date"
                                placeholder="select date"
                                format="YYYY-MM-DD"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                    },
                                    dateInput: {
                                    marginLeft: 36
                                    }
                                }}
                                onDateChange={(date) => {this.setState({TanggalPanen: date})}}
                                />
                            </Col>
                        </Row>

                        <Row>
                            <Col style={{ flex: 0.12, justifyContent:'center' }} >
                                <Icon name="md-basket" style={styles.iconForm} />
                            </Col>
                            <Col>
                                <TextField
                                    label={$t('Drying Day')}
                                    highlightColor={'#45be5c'}
                                    dense={true}
                                    value={this.state.HariKering}
                                    onChangeText={(text) => this.setState({ HariKering: text })}
                                />
                            </Col>
                        </Row>
                        

                        <Row>
                            <Col style={{ flex: 0.2, justifyContent:'center' }} >
                                <Icon name="md-basket" style={styles.iconForm} />
                            </Col>
                            <Col>
                                <TextField
                                    label={'# '+$t('Dry Beans')}
                                    highlightColor={'#45be5c'}
                                    dense={true}
                                    value={this.state.BijiKering}
                                    keyboardType="numeric"
                                    onChangeText={(text) => this.setState({ BijiKering: text })}
                                />
                            </Col>
                        
                            <Col style={{ flex: 0.2, justifyContent:'center' }} >
                                <Icon name="md-basket" style={styles.iconForm} />
                            </Col>
                            <Col>
                                <TextField
                                    label={'# '+$t('Wet Beans')}
                                    highlightColor={'#45be5c'}
                                    keyboardType="numeric"
                                    dense={true}
                                    value={this.state.BijiBasah}
                                    onChangeText={(text) => this.setState({ BijiBasah: text })}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{flex: 0.5, justifyContent: 'center'}}>
                            <Row>
                                <Col style={{ flex: 0.3, justifyContent:'center' }} >
                                    <Icon name="md-calendar" style={styles.iconForm} />
                                </Col>
                                <Col style={{ justifyContent: 'center'}}>
                                    <Text style={{ color: '#7d7d7d', fontSize: 14, marginTop:10, marginLeft:10 }}>
                                   {$t('Sale Date')}
                                    </Text>
                                </Col>
                                </Row>
                            </Col>
                            <Col>
                                <DatePicker
                                style={{width: 200,top:10}}
                                date={this.state.TanggalPenjualan}
                                showIcon={false}
                                mode="date"
                                placeholder="select date"
                                format="YYYY-MM-DD"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                    },
                                    dateInput: {
                                    marginLeft: 36
                                    }
                                }}
                                onDateChange={(date) => {this.setState({TanggalPenjualan: date})}}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{ flex: 0.12, justifyContent:'center' }} >
                                <Icon name="md-basket" style={styles.iconForm} />
                            </Col>
                            <Col>
                                <TextField
                                    label={$t('Price')}
                                    highlightColor={'#45be5c'}
                                    dense={true}
                                    keyboardType="numeric"
                                    value={this.state.Harga}
                                    onChangeText={(text) => this.setState({ Harga: Format.formatNumber(text) })}
                                />
                            </Col>
                        </Row>


                        <Row>
                            <Col style={{ flex: 0.12, justifyContent:'center' }} >
                                <Icon name="md-basket" style={styles.iconForm} />
                            </Col>
                            <Col>
                                <TextField
                                    label={$t('Total Amount')}
                                    keyboardType="numeric"
                                    highlightColor={'#45be5c'}
                                    dense={true}
                                    value={this.state.Total}
                                    onChangeText={(text) => this.setState({ Total: Format.formatNumber(text) })}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{ flex: 0.1}} >
                                    <Icon name="md-book" style={styles.iconForm} />
                            </Col>
                            
                            <Col style={{top: 13, paddingBottom:13}}>
                                <TextInput
                                    multiline={true}
                                    numberOfLines={4}
                                    placeholder={$t('Description')}
                                    onChangeText={(text) => this.setState({Keterangan: text})}
                                    style={{ borderColor:'#ccc',borderWidth: 1,textAlignVertical:'top'}}    
                                    value={this.state.Keterangan}/>
                            </Col>
                        </Row>
                </Grid>
                    </Content>
                </CardItem>
            </Card>
        </Content>
        <Footer style={{ height: 70, backgroundColor: '#8c8c85' }} >
                <Grid>
                    <Col>
                        <Button block success style={{margin: 15}} onPress={() =>  this.save()}>
                        <Icon active name="md-create" style={{ fontSize: 30, lineHeight: 32 }} />
                        <Text>{$t('Save')}</Text>
                        </Button>
                    </Col>
                    { this.deleteButton }
                </Grid>
        </Footer>
</Container>
        )
    }
}

function bindAction(dispatch) {
    return {
      popRoute: key => dispatch(popRoute(key)),
      pushRoute: (route, key) => dispatch(pushRoute(route, key)),
    };
  }
  
  const mapStateToProps = state => ({
    navigation: state.cardNavigation,
  });
  
  export default connect(mapStateToProps, bindAction)(WowFormIncome);
