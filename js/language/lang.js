exports.en = {};

const dashboard = {
    Certified: 'Sertifikasi',
    'Not Certified': 'Non Sertifikasi',
    'Number of Farmers': 'Jumlah Petani',
    Transaction: 'Transaksi',
    Weight: 'Berat',
    Amount: 'Jumlah',
    Search: 'Cari',
    Hide: 'Tutup'
};


const sideBar = {
    Dashboard: 'Beranda',
    Profile: 'Profil',
    Products: 'Produk',
    Business: 'Bisnis',
    Report: 'Laporan',
    Setting: 'Pengaturan',
}

const customer = {
    'Farmer Name': 'Nama Petani',
    'Farmer ID': 'ID Petani',
    'Phone': 'Telepon',
    'Address': 'Alamat',
    'Note': 'Catatan',
    'Save': 'Simpan',
    'Search by Name / ID': 'Cari Nama/ID Petani',
    'Village': 'Desa',
}

const coaching = {
    Issue: 'Isu',
    'CVC Implementation': 'CVC Implementasi',
    Recomendation: 'Rekomendasi',
    'Date End': 'Tanggal Selesai',
    'Date Coaching': 'Tanggal Coaching',
}

const transaction = {
    Qty: 'Kuantitas',
    Add: 'Tambah',
    'Payment Detail': 'Detil Pembayaran',
    Discount: 'Diskon',
    Payment: 'Pembayaran',
    Cashback: 'Sisa Bayar',
    'Transaction Detail': 'Detil Transaksi',
}

const bisnis = {
    Expense: 'Pengeluaran',
    Income: 'Pendapatan',
    'Wow Farm': 'Kebun Wow',
    Price: 'Harga',
    'Total Amount': 'Total Harga',
    Date: 'Tanggal',
    Description: 'Deskripsi',
    'Harvest Date': 'Tanggal Panen',
    'Drying Day': 'Hari Kering',
    'Dry Beans': 'Biji Kering',
    'Wet Beans': 'Biji Basah',
    'Sale Date': 'Tanggal Penjualan',
    'Name': 'Nama',
    'Unit Type': 'Satuan',
    'Serial Number': 'Nomer Seri',
    'Active': 'Aktif',
    'Inactive': 'Non-Aktif',
    'Data Information': 'Data Informasi'
}

const report = {
    'Profit & Loss': 'Laba & Rugi',
    'Sales Transaction': 'Sales Transaksi',
    'WowFarm Income': 'Pendapatan Kebun Wow',
    'Sales Expense': 'Pengeluaran',
    'WowFarm Expense': 'Pengeluaran Kebun Wow',
    'Date Range': 'Tanggal',
    Revenue: 'Pendapatan',
    Account: 'Akun'
}

const sync = {
    'Download Farmer': 'Unduh Petani',
    'Restore Data': 'Kembalikan Data'
}

const profile = {
    Age: 'Umur',
    Gender: 'Jenis Kelamin',
    'Garden Land Size': 'Luas Kebun',
    'Number of Cacao tres':'Jumlah Pohon',
    Male: 'Pria',
    Female: 'Wanita',
}

const setting = {
    Language: 'Bahasa',
    Logout: 'Keluar',
    Delete: 'Hapus',
    Configuration: 'Konfigurasi',
}

exports.id = Object.assign({}, dashboard, sideBar, customer, coaching, transaction, bisnis, report, sync, profile, setting);
