import React, { Component } from 'react';

import { NetInfo, Text } from 'react-native';
import PushData from '../actions/pushdata';

import DBconnect from '../database/db';

const db = new DBconnect();

export default class ConnectionInfo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isConnect: false,
    };
  }

  componentDidMount() {
    NetInfo.addEventListener('change', this._checkConnection.bind(this));
    NetInfo.fetch().done(connection => this.setState({ isConnect: connection }));
  }

  async syncronize() {
    await this.dbFarmer();
    await this.dbCoaching();
    await this.dbInventory();
    await this.dbTransactionCustomer();
    await this.dbCvcInOut();
    await this.dbWowIn();
    await this.dbWowOut();
  }

  async dbCvcInOut() {
    const queryTable = await db.table('cvc_inoutcome').findAll({ synced: 0 });
    for (let i = 0; i < queryTable.length; i++) {
      let body = { 
        data: queryTable.item(i) 
      };
      const results = await new PushData().endpoint('cocoadoctor/cvcSync').data(body).post();
      if (typeof results !== 'undefined') {
        const obj = {
            cvcSyncID: results.data.cvcSyncID,
            synced: '1',
          };
        await db.table('cvc_inoutcome').save({ cvcSyncID: results.data.cvcSyncID }, obj);
      }
    }
  }

  async dbWowIn() {
    const queryTable = await db.table('wow_income').findAll({ synced: 0 });
    for (let i = 0; i < queryTable.length; i++) {
      let body = { 
        data: queryTable.item(i) 
      };
      const results = await new PushData().endpoint('cocoadoctor/wowInSync').data(body).post();
      if (typeof results !== 'undefined') {
        const obj = {
            wowInSyncID: results.data.wowInSyncID,
            synced: '1',
          };
        await db.table('wow_income').save({ wowInSyncID: results.data.wowInSyncID }, obj);
      }
    }
  }

  async dbWowOut() {
    // wow_outcome
    const queryTable = await db.table('wow_outcome').findAll({ synced: 0 });
    for (let i = 0; i < queryTable.length; i++) {
      let body = { 
        data: queryTable.item(i) 
      };
      // console.log(JSON.stringify(body));
      const results = await new PushData().endpoint('cocoadoctor/wowOutSync').data(body).post();
      if (typeof results !== 'undefined') {
        const obj = {
            wowOutSyncID: results.data.wowOutSyncID,
            synced: '1',
          };
        await db.table('wow_outcome').save({ wowOutSyncID: results.data.wowOutSyncID }, obj);
      }
    }
  }

  async dbInventory() {
    const queryTable = await db.table('cd_inventory').findAll({ synced: 0 });
    for (let i = 0; i < queryTable.length; i++) {
      let body = { 
        data: queryTable.item(i) 
      };
      const results = await new PushData().endpoint('cocoadoctor/inventorySync').data(body).post();
      if (typeof results !== 'undefined') {
        const obj = {
            InventorySyncID: results.data.InventorySyncID,
            synced: '1',
          };
        await db.table('cd_inventory').save({ InventorySyncID: results.data.InventorySyncID }, obj);
      }
    }
  }

  async dbFarmer() {
    const queryTable = await db.table('cd_customer').findAll({ synced: '0' });
    for (let i = 0; i < queryTable.length; i++) {
        const body = queryTable.item(i);
        const results = await new PushData().endpoint('cocoadoctor/farmerSync').data(body).post();
        if (typeof results !== 'undefined') {
            const obj = {
                CustomerSyncID: results.data.CustomerSyncID,
                synced: '1',
              };
            await db.table('cd_customer').save({ CustomerSyncID: results.data.CustomerSyncID }, obj);
          }
      }
  }

  async dbTransactionCustomer() {
    const queryCustomer = await db.table('cd_transaction_customer').findAll({ synced: 0 });
    const detailCustomer = [];
    for (let i = 0; i < queryCustomer.length; i++) {
        const dataCustomer = queryCustomer.item(i);
        const queryCustomerDetail = await db.table('cd_transaction_customer_detail').findAll({ transactionCustomerId: dataCustomer.SaleSyncID });
        const body = { data: queryCustomer.item(i) };
        const bodyCustomerDetails = { 
          data: {
            SaleSyncID: dataCustomer.SaleSyncID,
            details: (queryCustomerDetail.length > 0) ? JSON.parse(queryCustomerDetail.item(0).details) : [] ,
          },
        };
        
        const results = await new PushData().endpoint('cocoadoctor/transSyncCustomer').data(body).post();
        const results2 = await new PushData().endpoint('cocoadoctor/transSyncCustomerDetails').data(bodyCustomerDetails).post();
        if (typeof results !== 'undefined') {
            await db.table('cd_transaction_customer').save({ SaleSyncID: results.data.SaleSyncID }, { synced: 1 });
          }
      }
  }

  async dbCoaching() {
    const queryTable = await db.table('cd_coaching').findAll({ synced: '0' });
    for (let i = 0; i < queryTable.length; i++) {
      
      const body = { data: queryTable.item(i) };
      const results = await new PushData().endpoint('cocoadoctor/coachingSync').data(body).post();
      if (typeof results !== 'undefined') {
          await db.table('cd_coaching').save({ CoachingSyncID: results.data.CoachingSyncID }, { synced: 1 });
        }
      }
  }

  _checkConnection(connection) {
    let c;
    if (connection) {
        NetInfo.isConnectionExpensive()
            .then((isConnectionExpensive) => {
              if (connection != 'NONE') {
                c = isConnectionExpensive ? 'Low Speed' : 'Connected';
                this.syncronize();
              } else {
                c = connection;
              }

              this.setState({
                isConnect: c,
              });
              console.log(c);
            })
            .catch((error) => {
              console.error(error);
            });
      }
  }

  componentWillUnmount() {
    NetInfo.removeEventListener(
        'change',
        this._checkConnection.bind(this)
        );
  }

  render() {
    { return (this.state.isConnect === 'NONE' && <Text style={{ textAlign: 'center', backgroundColor: '#3b3c40', color: 'white' }}>No Internet/Network Connection</Text>); }
  }
}
