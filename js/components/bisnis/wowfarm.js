import React, { Component } from 'react';
import { Alert, TextInput, Dimensions, ListView } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';

import { Container, Thumbnail, Header, Title, Content, Text, List, ListItem, Badge, Footer, Button, Icon, Left, Body, Right, Form, Label, Item, Input, Card, CardItem } from 'native-base';
import DBconnect from '../../database/db';
import Format from '../../helpers/Format';

const db = new DBconnect();

const income = require('../../../images/income.png');
const outcome = require('../../../images/outcome.png');

export default class ListWowfarm extends Component {
    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            title: 'Bisnis',
            dataIncomeWow: [],
        }
    }

    componentWillReceiveProps() {
        this.componentDidMount();
    }

    async componentDidMount() {
        let dataInOutcome = []
        let dataWowIncome = await db.table('wow_income').findAll({ StatusRevision: '' });
        let dataWowOutcome = await db.table('wow_outcome').findAll({ StatusRevision: '' });
        for (let i = 0; i < dataWowIncome.length; i++) {
            dataInOutcome.push(dataWowIncome.item(i));
        }
        for (let z = 0; z < dataWowOutcome.length; z++) {
            dataInOutcome.push(dataWowOutcome.item(z));
        }
        this.setState({
            dataIncomeWow: dataInOutcome,
        });
    }

    routeTo(route) {
        this.props.route(route);
    }
    render() {
            
        return(
            <ListView
                    enableEmptySections={true}
                    dataSource={this.ds.cloneWithRows(this.state.dataIncomeWow)}
                    renderRow={(rowData) => 
                        <List style={{ backgroundColor: '#fff' }}>
                            <ListItem style={{ height: 70}} onPress={() => {
                                    if(typeof rowData.wowInSyncID != 'undefined') { // income
                                        global.wowIncomeID = rowData.id;
                                        return this.routeTo('wow-farm-income');
                                    } else {
                                        global.wowOutComeID = rowData.id;
                                        return this.routeTo('bisnis-outcome-form');
                                    }
                                }}>
                                <Grid>
                                    <Col style={{ justifyContent: 'center', flex: 0.5 }}>
                                          {rowData.synced == 1 ?  
                                          <Icon name="md-cloud-done" style={{ color: '#3e9ab5',fontSize: 40}}/>
                                          :
                                          <Icon name="ios-cloud-outline" style={{ color: '#3e9ab5', fontSize: 40}}/>
                                          }
                                    </Col>
                                    <Col style={{ flex: 1.3, justifyContent: 'center' }}>
                                        <Text style={{color: 'black' }}>{rowData.TanggalPanen || rowData.Tanggal }</Text>
                                    </Col>
                                    <Col style={{flexDirection: "row", flex: 1.5, justifyContent: 'center' }}>
                                        <Thumbnail
                                                style={{ height: 30, width: 30 }}
                                                source={(rowData.TanggalPanen != undefined) ? income :outcome}
                                              />
                                        <Text style={{ alignSelf: 'center', backgroundColor: '#80b3a5', padding: 7, borderRadius: 10 }}>{Format.formatNumber(rowData.Total)}</Text>
                                    </Col>
                                </Grid>
                            </ListItem>
                        </List>
                    }
                />

        )
    }
}
